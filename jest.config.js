module.exports = {
  projects: [
    '<rootDir>/libs/angular',
    '<rootDir>/libs/shared',
    '<rootDir>/libs/media',
    '<rootDir>/libs/nest-keycloak',
    '<rootDir>/libs/nest',
    '<rootDir>/apps/api',
    '<rootDir>/apps/cli',
    '<rootDir>/libs/blog',
    '<rootDir>/apps/demo',
  ],
};
