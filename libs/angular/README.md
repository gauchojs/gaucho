# @gaucho/angular

The angular component library

## NggComponent

```ts
import { Component } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NggComponent } from '@gaucho/angular';

@Component({
  selector: 'app-home',
  template: `<article *ngFor="let post of posts$ | async">{{post.title}}</article>`,
})
class AppHome extends NggComponent implements OnInit {

  posts$: Obeservable<any[]>;

  constructor(private http: HttpClient) { }

  ngOnInit() {

    this.posts$ = this.http.get('api/posts')
    .pipe(
      takeUntil(this.destroyed$)
    );

  }

}
```
