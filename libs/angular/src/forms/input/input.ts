import { Component, Input, ViewChild, ElementRef, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NggInputBase } from '../shared';

@Component({
  selector: 'ngg-input',
  template: `
  <label [for]="name" *ngIf="label">{{label}}</label>
  <input
    class="form-control {{inputClass}}"
    [name]="name"
    [id]="name"
    [type]="type"
    #input
    (input)="onChange($event.target.value)"
    (blur)="onTouched()"
    [disabled]="disabled"
    attr.aria-describedby="help{{name}}"
    [placeholder]="placeholder"
    [class.is-invalid]="ctrl?.invalid && ctrl?.touched"
    [class.is-valid]="ctrl?.valid && ctrl?.dirty"
    >

  <!-- Errors -->
  <div class="invalid-feedback" *ngFor="let item of errorsMessages">
    {{item.message}}
  </div>

  <!-- Help text -->
  <small id="help{{name}}" class="form-text text-muted" *ngIf="help">
    {{help}}
  </small>
`,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => NggInput),
    multi: true
  }]
})
export class NggInput extends NggInputBase implements ControlValueAccessor {

  @Input() inputClass: string;

  @Input() type = 'text';

  @Input() placeholder = '';

  onChange: (obj: any) => void;

  onTouched: () => void;

  @ViewChild('input', { static: true }) input: ElementRef;

  constructor() {
    super('input');
  }

  writeValue(value: any): void {
    this.input.nativeElement.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

}
