import { AbstractControl, NgControl, FormControlName, FormControl } from '@angular/forms';
import { Input, OnInit, ContentChild, Injectable } from '@angular/core';
import { InputErrorDefinition } from './types';
import { inputErrors } from './errors';

@Injectable()
export abstract class NggInputBase implements OnInit {

  public controlDir: NgControl;

  @Input() label: string = null;

  @Input() help: string = null;

  @Input() customErrors: InputErrorDefinition[];

  private defaultsErrors: InputErrorDefinition[] = [];

  disabled: boolean;

  @ContentChild(FormControlName) private formControlNameRef: FormControlName;

  get name(): string {
    return this.formControlNameRef ? String(this.formControlNameRef.name) : 'input';
  }

  get ctrl(): AbstractControl {
    return this.formControlNameRef ? <FormControl>this.formControlNameRef.control : null;
  }

  constructor(private controlType: string) { }

  ngOnInit(): void {
    const raw: InputErrorDefinition[] = inputErrors[this.controlType];
    this.defaultsErrors =  raw.map(e => {
      if (e.name === 'invalid') {
        return { name: this.name, message: e.message };
      }
      return e;
    })
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  hasError(error = 'required'): boolean {
    if (!this.ctrl) {
      return false;
    }

    if (!this.ctrl.errors) {
      return false;
    }

    return this.ctrl.errors[error] !== undefined;
  }

  errorDefinitions(type: string = 'input'): InputErrorDefinition[] {
    // change invalid to control name
    if (!this.customErrors) {
      return this.defaultsErrors;
    }

    const res = this.customErrors;

    this.defaultsErrors.forEach(err => {
      if (!res.find(e => e.name === err.name)) {
        res.push(err);
      }
    });

    return res;
  }

  get errorsMessages(): InputErrorDefinition[] {
    return this.errorDefinitions().filter(e => this.hasError(e.name));
  }

}
