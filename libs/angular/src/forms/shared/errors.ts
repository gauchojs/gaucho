import { InputErrorDefinition } from './types';

const INPUT_ERRORS: InputErrorDefinition[] = [
  { name: 'required', message: 'Por favor, ingresá un valor' },
  { name: 'invalid', message: 'Por favor, ingresá un valor válido' },
  { name: 'maxlength', message: 'Demasiados caracteres' },
  { name: 'minlength', message: 'Por favor, ingresá mas caracteres' },
];

const SELECT_ERRORS: InputErrorDefinition[] = [
  { name: 'required', message: 'Por favor, seleccioná un valor' },
];

export const inputErrors = {
  input: INPUT_ERRORS,
  select: SELECT_ERRORS,
};
