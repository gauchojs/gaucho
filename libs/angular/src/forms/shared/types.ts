export interface InputErrorDefinition {
  name: string;
  message: string;
}

export interface SelectOption {
  value: any;
  caption: string;
}
