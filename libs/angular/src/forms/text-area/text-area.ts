import { Component,Self, Input, ViewChild, ElementRef, Optional, HostBinding, forwardRef } from '@angular/core';
import { ControlValueAccessor, NgControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NggInputBase } from '../shared';

@Component({
  selector: 'ngg-textarea',
  template: `
  <label [for]="name" *ngIf="label">{{label}}</label>
  <textarea
    class="form-control {{inputClass}}"
    [name]="name"
    [id]="name"
    #input
    (input)="onChange($event.target.value)"
    (blur)="onTouched()"
    [disabled]="disabled"
    attr.aria-describedby="help{{name}}"
    [placeholder]="placeholder"
    [class.is-invalid]="ctrl?.invalid && ctrl?.touched"
    [class.is-valid]="ctrl?.valid && ctrl?.dirty"
    >
  </textarea>
  <!-- Errors -->
  <div class="invalid-feedback" *ngFor="let item of errorsMessages">
    {{item.message}}
  </div>

  <!-- Help text -->
  <small id="help{{name}}" class="form-text text-muted">
    {{help}}
  </small>
  `,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => NggTextArea),
    multi: true
  }]
})

export class NggTextArea extends NggInputBase implements ControlValueAccessor {

  @Input() inputClass;

  @Input() placeholder = '';

  onChange: (obj: any) => void;

  onTouched: () => void;

  @ViewChild('input', { static: true }) input: ElementRef;

  constructor() {
    super('input');
  }

  writeValue(value: any): void {
    this.input.nativeElement.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

}
