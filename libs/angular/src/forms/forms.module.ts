import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NggInput } from './input';
import { NggTextArea } from './text-area';

const COMPONENTS = [
  NggInput,
  NggTextArea
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class NggFormsModule { }
