import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { NggFormsModule } from './forms.module';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

@Component({
  selector: 'test-input',
  template: `
    <div id="nolabel">
      <ngg-input></ngg-input>
    </div>
    <div id="labeled">
      <ngg-input label="Test"></ngg-input>
    </div>
    <div id="help">
      <ngg-input label="Help" help="help text"></ngg-input>
    </div>
  `,
})
class TestInput { }

@Component({
  selector: 'test-input',
  template: `
    <div id="nolabel">
      <ngg-textarea></ngg-textarea>
    </div>
    <div id="labeled">
      <ngg-textarea label="Test"></ngg-textarea>
    </div>
    <div id="help">
      <ngg-textarea label="Help" help="help text"></ngg-textarea>
    </div>
  `,
})
class TestTextarea { }

@Component({
  selector: 'test-form',
  template: `
    <form [formGroup]="formGroup">
      <ngg-input formControlName="title"></ngg-input>
      <ngg-textarea formControlName="summary"></ngg-textarea>
    </form>
  `,
})
class TestForm {

  formGroup = new FormGroup({
    title: new FormControl(null, [Validators.required]),
    summary: new FormControl(null, [Validators.required])
  });

}

describe('NggForms module', () => {

  let input: TestInput;
  let inputFixture: ComponentFixture<TestInput>;

  let textarea: TestTextarea;
  let textareaFixture: ComponentFixture<TestTextarea>;

  let form: TestForm;
  let formFixture: ComponentFixture<TestForm>;

  beforeEach(async () => {

    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(
      BrowserDynamicTestingModule,
      platformBrowserDynamicTesting()
    );

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        NggFormsModule
      ],
      declarations: [
        TestInput,
        TestForm,
        TestTextarea
      ],
    });

    await TestBed.compileComponents();

    // Input
    inputFixture = TestBed.createComponent(TestInput);
    input = inputFixture.componentInstance;
    inputFixture.detectChanges();
    // Textarea
    textareaFixture = TestBed.createComponent(TestInput);
    textarea = textareaFixture.componentInstance;
    textareaFixture.detectChanges();
    // FormControl
    formFixture = TestBed.createComponent(TestForm);
    form = formFixture.componentInstance;
    formFixture.detectChanges();
  });

  describe('NggInput', () => {

    it('should instantiate', () => {
      expect(input).toBeDefined();
    });

    it('should have an input with no label', () => {
      const elem: HTMLElement = inputFixture.debugElement.nativeElement;
      const icmp = elem.querySelector('#nolabel input');
      const ilabel: HTMLLabelElement = elem.querySelector('#nolabel label');
      expect(icmp).toBeDefined();
      expect(ilabel).toBe(null);
    });

    it('should have an input with a label', () => {
      const elem: HTMLElement = inputFixture.debugElement.nativeElement;
      const icmp = elem.querySelector('#labeled input');
      const ilabel: HTMLLabelElement = elem.querySelector('#labeled label');

      expect(icmp).toBeDefined();
      expect(ilabel).toBeDefined();
      expect(ilabel.innerHTML).toBe('Test');
    });

    it('should have an input with a help', () => {
      const elem: HTMLElement = inputFixture.debugElement.nativeElement;
      const icmp = elem.querySelector('#help input');
      const ihelp: HTMLElement = elem.querySelector(`#help #helpinput`);

      expect(icmp).toBeDefined();
      expect(ihelp).toBeDefined();
      expect(ihelp.innerHTML.trim()).toBe('help text');
    });

  });

  describe('NggTextarea', () => {

    it('should instantiate', () => {
      expect(textarea).toBeDefined();
    });

    it('should have an input with no label', () => {
      const elem: HTMLElement = textareaFixture.debugElement.nativeElement;
      const icmp = elem.querySelector('#nolabel input');
      const ilabel: HTMLLabelElement = elem.querySelector('#nolabel label');
      expect(icmp).toBeDefined();
      expect(ilabel).toBe(null);
    });

    it('should have an input with a label', () => {
      const elem: HTMLElement = textareaFixture.debugElement.nativeElement;
      const icmp = elem.querySelector('#labeled input');
      const ilabel: HTMLLabelElement = elem.querySelector('#labeled label');

      expect(icmp).toBeDefined();
      expect(ilabel).toBeDefined();
      expect(ilabel.innerHTML).toBe('Test');
    });

    it('should have an input with a help', () => {
      const elem: HTMLElement = textareaFixture.debugElement.nativeElement;
      const icmp = elem.querySelector('#help input');
      const ihelp: HTMLElement = elem.querySelector(`#help #helpinput`);

      expect(icmp).toBeDefined();
      expect(ihelp).toBeDefined();
      expect(ihelp.innerHTML.trim()).toBe('help text');
    });

  });

  describe('NggInput/NggTextarea with reactive form', () => {

    it('should instantiate', () => {
      expect(form).toBeDefined();
    });

    it('should have an input with a valid name and id', () => {
      const elem: HTMLElement = formFixture.debugElement.nativeElement;
      const iByName: HTMLElement = elem.querySelector('input[name=title]');
      const iById: HTMLElement = elem.querySelector('input#title');
      expect(iByName).toBeDefined();
      expect(iById).toBeDefined();
    });

    it('should have an textarea with a valid name and id', () => {
      const elem: HTMLElement = formFixture.debugElement.nativeElement;
      const tiByName: HTMLElement = elem.querySelector('textarea[name=summary]');
      const tiById: HTMLElement = elem.querySelector('textarea#summary');
      expect(tiByName).toBeDefined();
      expect(tiById).toBeDefined();
    });

    it('should handle errors', () => {
      const elem: HTMLElement = formFixture.debugElement.nativeElement;
      form.formGroup.markAllAsTouched();
      formFixture.detectChanges();
      const invalid: NodeList = elem.querySelectorAll('.invalid-feedback');
      expect(invalid.length).toBe(2);
    });

  });

})
