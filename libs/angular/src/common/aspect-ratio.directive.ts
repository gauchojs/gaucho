import { Directive, Input, ElementRef, Renderer2, OnInit, AfterContentChecked } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Directive({
  selector: '[nggAspectRatio]'
})
export class NggExpandRatio implements OnInit, AfterContentChecked {

  private width$ = new Subject<number>();

  constructor(
    private element: ElementRef,
    private render: Renderer2,
  ) {
  }

  ngOnInit() { }

  ngAfterContentChecked() {
    this.width$.next((<HTMLElement>this.element.nativeElement).offsetWidth);
  }

}
