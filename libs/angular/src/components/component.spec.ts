import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Component } from "@angular/core";
import { NggComponent } from './component';

@Component({
  selector: '.view',
  template: `<div>Test</div>`,
})
class ViewComponent extends NggComponent {
}

describe('NggComponent', () => {

  let component: ViewComponent;
  let fixture:   ComponentFixture<ViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewComponent ],
    });
    fixture = TestBed.createComponent(ViewComponent);
    component = fixture.componentInstance;
  }));

  it('should destroyed$ on ngDestory', (done) => {

    component.destroyed$.subscribe(
      destroy => expect(destroy).toBe(true),
      () => {},
      done
    );
    fixture.destroy();

  });

})
