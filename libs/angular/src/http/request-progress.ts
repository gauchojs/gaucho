import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { shareReplay, takeUntil } from 'rxjs/operators';
import { HttpErrorResponse, HttpEvent } from '@angular/common/http';

export type RequestProgressMode = 'determinate' | 'indeterminate';

export class RequestProgress<T> extends Subject<T> {

  private destroy$: Subject<boolean> = new Subject();

  private $mode = new BehaviorSubject<RequestProgressMode>('indeterminate');
  mode$ = this.$mode.asObservable().pipe(
    takeUntil(this.destroy$),
    shareReplay()
  );

  private $progress = new  BehaviorSubject<number>(0);
  progress$ = this.$progress.asObservable().pipe(
    takeUntil(this.destroy$),
    shareReplay()
  );

  private $bussy = new  BehaviorSubject<boolean>(false);
  bussy$ = this.$bussy.asObservable().pipe(
    takeUntil(this.destroy$),
    shareReplay()
  );

  startTime: number;

  endTime: number;

  constructor(private request: Observable<HttpEvent<T>>) {
    super();

    request.pipe(
      takeUntil(this.destroy$),
    )
  }

  start() {
    this.$bussy.next(true);
    this.startTime = new Date().getTime();
  }

  update(value: number, mode: RequestProgressMode = 'determinate') {
    this.$progress.next(value);
    this.$mode.next(mode);
  }

  complete(data?: T) {
    this.$bussy.next(false);
    this.update(100, 'indeterminate');
    this.endTime = new Date().getTime();
    this.complete(data);

    this.destroy();
  }

  fail(error: HttpErrorResponse) {
    this.$bussy.next(false);
    this.update(100, 'indeterminate');
    this.endTime = new Date().getTime();
    this.error(error);

    this.destroy();
  }

  private destroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
