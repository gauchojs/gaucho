# @gaucho/nest/repository

## Description

Usefull module to work with mongoose

## Installation

```bash
$ npm i --save @gaucho/nest @nestjs/mongoose mongoose
```

## Usage

` app.module.ts`
```ts
import { Module } from "@nestjs/common";
import { RepositoryModule } from '@gaucho/nest/repository';
import { UsersModule } './users.module';

@Module({
  imports: [
    RepositoryModule.forRoot('mongodb://localhost:27017/somedb'),
    UsersModule,
  ]
})
export class AppModule { }
```

`users.interface.ts`
```ts
export interface User {
  username: string,
  password?: String,
  roles?: string[],
}
```

`users.schema.ts`
```ts
export const UserSchema = new Schema({
  username: String,
  password: String,
  roles: [String],
});
```

`users.module.ts`
```ts
@Module({
  imports: [
    RepositoryModule.forFeature([{
      name: 'user',
      schema: UserSchema,
      hidden: [ 'password' ]
    }])
  ],
  controllers: [ UsersController ],
})
class UsersModule {  }
```

`users.controller.ts`
```ts
import { Controller, Get, Post, Put, Delete, Query, Param, Body } from '@nestjs/common';
import { InjectRespository, Repository } from '@gaucho/nest/repository';
import { User } from './users.interface.ts'

@Controller('users')
export class UsersController {

  constructor(
    @InjectRespository('user')
    private userRepository: Repository<User>
  ){}

  @Get()
  find(@Query('page') page?: string, @Query('size') size?: string) {
    if (page) {
      const pn = Number(page);
      const sizen = Number(size) || 10;
      return this.repo.paginate({}, pn, sizen);
    }
    return this.repo.find({});
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.repo.findById(id);
  }

  @Post()
  create(@Body() data: Partial<User>) {
    return this.repo.create(data);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() data: Partial<User>) {
    return this.repo.update(id, data);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.repo.delete(id);
  }

}

```

## The Repository Service with a [{json:api}](https://jsonapi.org/) approach

```ts
import { RepositoryService } from '@gaucho/nest/repository';

export class ApiService {

  constructor(repository: RepositoryService) { }

  findArticles(title: string) {
    const query = { type: 'articles', filter: { title } };
    const include = { relationship: 'author' };
    return this.repository.find(query, include);
  }

}
```
