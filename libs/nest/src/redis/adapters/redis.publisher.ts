import { RedisClient } from '../ifaces';

export class RedisPublisher<T> {

  /**
   *
   * Create a new RedisPublisher<T>
   *
   * @param client for publish message
   */
  constructor(public channel, private client: RedisClient) {
  }

  /**
   *
   * Publish the `data` into the channel
   *
   * @param data any kind of data (an recursive json is not supported)
   */
  async publish(data: T): Promise<number> {
    return this.client.publish(this.channel, JSON.stringify(data));
  }

}
