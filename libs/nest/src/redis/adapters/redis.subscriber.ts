import { RedisClient } from '../ifaces';
import { Subject } from 'rxjs';

export class RedisSubscriber<T> extends Subject<T> {

  /**
   *
   * Create a new RedisSubscriber<T> is a Subject<T>
   *
   * @param client for get the messages
   */
  constructor(public channel, private client: RedisClient) {
    super();
    this.initialize();
  }

  private async initialize() {
    await this.client.subscribe(this.channel);
    this.client.on('message',
      (chl, message) => {
        if (chl === this.channel)  {
          this.next(JSON.parse(message));
        }
      }
    );
  }

}
