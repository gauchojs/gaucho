import { RedisClient, RedisMode } from '../ifaces';

export class RedisStore {

  /**
   * Create a new RedisStore
   *
   * @param client the redis client
   * @param expire Default expiration time in seconds
   */
  constructor(public client: RedisClient, private expire?: number, private mode: RedisMode = RedisMode.EX) { }

  /**
   *
   * Get the value for the `key`
   *
   * @param key sotred key
   *
   * @returns value as T or null if not exist
   */
  async get<T>(key: string): Promise<T> {
    const raw = await this.client.get(key);

    if (!raw) return null;

    const obj = JSON.parse(raw);

    return obj.val;
  }

  /**
   *
   * Add/override a key into the store
   *
   * @param key to store key
   * @param val value
   * @param expire overrides the default expiration time in second
   * @param expire overrides the mode
   */
  async set(key: string, val: any, expire?: number, mode: string = this.mode): Promise<'OK'> {
    const raw = JSON.stringify({ val });
    const exp = expire || this.expire;
    if (exp) {
      return this.client.set(key, raw, mode, exp);
    }

    return this.client.set(key, raw);
  }

  /**
   *
   * Check if exist a key in the store
   *
   * @param key the stored key
   */
  async exists(key: string): Promise<boolean> {
    const count = await this.client.exists(key);
    return count > 0;
  }

  /**
   * Delete the stored `key`
   *
   * @param key  the stored key
   */
  del(key: string): Promise<number> {
    return this.client.del(key);
  }

}
