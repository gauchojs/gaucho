import { Global, Module, DynamicModule, OnModuleDestroy } from '@nestjs/common';
import { RedisModuleOptions } from './ifaces/redis.options';
import { RedisService } from './services/redis.service';
import { ModuleRef } from '@nestjs/core';

@Module({
  providers: [RedisService],
  exports: [RedisService],
})
export class RedisCoreModule implements OnModuleDestroy {

  constructor(
    private readonly moduleRef: ModuleRef
  ) { }

  private static default: RedisModuleOptions = { url: 'redis://localhost:6379' };

  static forRoot(ops: RedisModuleOptions): DynamicModule {
    const mops = { ...ops, ...this.default };
    const options = { provide: RedisModuleOptions, useValue: new RedisModuleOptions(mops) };
    return {
      global: true,
      module: RedisCoreModule,
      providers: [
        options
      ],
      exports: [
        options
      ],
    };
  }

  onModuleDestroy(): Promise<void> {
    const service = this.moduleRef.get<RedisService>(RedisService);
    return service.destroy();
  }

}
