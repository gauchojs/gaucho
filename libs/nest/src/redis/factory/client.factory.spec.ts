import { createRedisClient } from "./client.factory";
import Redis from 'ioredis';
import { RedisClient } from '../ifaces';

describe('Redis Factory', () => {

  const keyPrefix = 'test';
  let client: RedisClient;
  let ioClient: RedisClient;

  beforeAll(async () => {
    ioClient = new Redis({ host: 'localhost', port: 6379 });
  });

  afterAll(async () => {
    if (client) {
      client.disconnect();
      ioClient.disconnect();
    }
  });

  it(`should handle redis stuff`, async () => {

    client = createRedisClient(`redis://localhost:6379/`, { keyPrefix });
    expect(typeof client).toBe('object');

    const result = await client.set('foo', 'bar');
    expect(result).toBe('OK');

    const foo = await ioClient.get(`${keyPrefix}foo`);
    expect(foo).toBe('bar');

    const fooc = await client.get(`foo`);
    expect(fooc).toBe('bar');

    await client.del('foo');

    const exists = await ioClient.exists(`${keyPrefix}foo`);
    expect(exists).toBe(0);

  })


});
