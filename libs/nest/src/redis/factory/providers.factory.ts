import { RedisStoreOptions, RedisChannelOptions } from '../ifaces';
import { Provider } from '@nestjs/common';
import { getStoreToken, getSubscriberToken, getPublisherToken } from '../utils/token';
import { RedisService } from '../services/redis.service';


export function createStoreProviders(stores: RedisStoreOptions[] = []): Provider[] {
  const providers = (stores || []).map(store => ({
    provide: getStoreToken(store.name),
    useFactory: async (service: RedisService) => {
      if (!store.url) {
        return service.createStore(store.expire, store.mode, {keyPrefix: store.name, ...store.redisOptions});
      }
      return service.createStoreFor(store.url, store.expire, store.mode, { keyPrefix: store.name, ...store.redisOptions });
    },
    inject: [ RedisService ],
  }));
  return providers;
}

export function createChannelProviders(channels: RedisChannelOptions[] = []): Provider[] {
  const subscribers = (channels || []).map(channel => ({
    provide: getSubscriberToken(channel.name),
    useFactory: async (service: RedisService) => {
      service.createSubscriber(channel.name)
      if (!channel.url) {
        return service.createSubscriber(channel.name)
      }
      return service.createSubscriberFor(channel.url, channel.name);
    },
    inject: [ RedisService ],
  }));
  const publishers = (channels || []).map(channel => ({
    provide: getPublisherToken(channel.name),
    useFactory: async (service: RedisService) => service.createPublisher(channel.name),
    inject: [ RedisService ],
  }));
  return [...subscribers, ...publishers];
}
