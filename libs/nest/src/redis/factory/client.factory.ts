import Redis from 'ioredis';
import { RedisOptions } from 'ioredis';
import { RedisClient } from '../ifaces/redis.client';

/**
 *
 * Create a simple RedisClient (not need import the RedisModule), for example a client for express-session
 *
 * @param url The redis url
 * @param redisOptions Extra redis comfiguration for ioredis
 *
 * @return RedisClient  a redis client
 */
export function createRedisClient(url: string = 'redis://localhost:6379', redisOptions: RedisOptions = {}): RedisClient {
  return new Redis(url, redisOptions);
}
