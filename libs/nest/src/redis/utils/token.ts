export function getStoreToken(index: string) {
  return `${index}RedisStore`;
}

export function getSubscriberToken(index: string) {
  return `${index}RedisSubscriber`;
}

export function getPublisherToken(index: string) {
  return `${index}RedisPublisher`;
}
