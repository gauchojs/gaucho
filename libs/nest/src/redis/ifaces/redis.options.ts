import { RedisOptions } from 'ioredis';

export class RedisModuleOptions {

  url = 'redis://localhost:6379';
  extra?: RedisOptions;

  constructor(ops?: any) {
    Object.assign(this, ops);
  }
}
