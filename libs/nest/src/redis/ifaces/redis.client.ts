import * as Redis from 'ioredis';

export type RedisClient = Redis.Redis;
