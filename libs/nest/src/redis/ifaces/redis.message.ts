export interface RedisMessage {
  readonly message: string;
  readonly channel: string;
}
