export interface RedisChannelOptions {
  name: string;
  url?: string;
}
