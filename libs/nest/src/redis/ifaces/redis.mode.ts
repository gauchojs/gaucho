export enum RedisMode {
  /**
   * seconds -- Set the specified expire time, in seconds.
   */
  EX = 'EX',

  /**
   * milliseconds -- Set the specified expire time, in milliseconds.
   */
  PX = 'PX',

  /**
   * -- Only set the key if it does not already exist.
   */
  NX = 'NX',

  /**
   * -- Only set the key if it already exist.
   */
  XX = 'XX',

  /**
   * -- Retain the time to live associated with the key.
   */
  KEEPTTL = 'KEEPTTL',

}
