import { RedisMode } from './redis.mode';

export interface RedisStoreOptions {
  name: string;
  expire?: number;
  mode?: RedisMode;
  url?: string;
  keyPrefix?: string;
  redisOptions?: any;
}
