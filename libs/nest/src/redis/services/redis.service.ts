import { Injectable, Optional } from '@nestjs/common';
import { RedisModuleOptions } from '../ifaces/redis.options';
import { RedisClient } from '../ifaces/redis.client';
import { createRedisClient } from '../factory/client.factory';
import { RedisStore } from '../adapters/redis.store';
import { from } from 'rxjs';
import { switchMap, concatMap } from 'rxjs/operators';
import { RedisSubscriber } from '../adapters/redis.subscriber';
import { RedisPublisher } from '../adapters/redis.publisher';
import { RedisMode } from '../ifaces';
import { RedisOptions } from 'ioredis';

@Injectable()
export class RedisService {

  private clients: RedisClient[] = [];

  constructor(private ops: RedisModuleOptions) { }

  private checkGlobal() {
    if (!this.ops) {
      throw new Error(`The global RedisOptions is not defined, use RedisModule.forRoot()`);
    }
  }

  /**
   *
   * Create a new RedisClient
   *
   * @param redisOptions extra configurations for ioredis
   */
  createClient(redisOptions: RedisOptions = {}): RedisClient {

    this.checkGlobal();

    const mops = {...this.ops.extra, ...redisOptions};

    const client = createRedisClient(this.ops.url, mops);

    this.clients.push(client);

    return client;
  }

  /**
   *
   * Create a new RedisClient
   *
   * @param redisOptions extra configurations for ioredis
   */
  createClientFor(url: string, redisOptions: RedisOptions = {}): RedisClient {

    const mops = {...this.ops.extra, ...redisOptions};

    const client = createRedisClient(url, mops);

    this.clients.push(client);

    return client;
  }

  createStore(expire?: number, mode: RedisMode = RedisMode.EX, redisOptions: RedisOptions = {}): RedisStore {
    this.checkGlobal();

    const client = this.createClient(redisOptions);

    return new RedisStore(client, expire, mode);
  }

  createSubscriber<T>(channel: string): RedisSubscriber<T> {
    const client = this.createClient();
    return new  RedisSubscriber<T>(channel, client);
  }

  createPublisher<T>(channel: string): RedisPublisher<T> {
    const client = this.createClient();
    return new  RedisPublisher<T>(channel, client);
  }

  createStoreFor(url: string, expire?: number, mode: RedisMode = RedisMode.EX, redisOptions: RedisOptions = {}): RedisStore {
    const client = this.createClientFor(url, redisOptions);

    return new RedisStore(client, expire, mode);
  }

  createSubscriberFor<T>(url: string, channel: string): RedisSubscriber<T> {
    const client = this.createClientFor(url);
    return new  RedisSubscriber<T>(channel, client);
  }

  createPublisherFor<T>(url: string, channel: string): RedisPublisher<T> {
    const client = this.createClientFor(url);
    return new  RedisPublisher<T>(channel, client);
  }

  async destroy(): Promise<void> {
    return from(this.clients)
      .pipe(
        concatMap(client => {
          client.disconnect();
          return Promise.resolve();
        }),
        switchMap(cli => Promise.resolve())
      ).toPromise();
  }

}
