import { Inject } from '@nestjs/common';
import { getStoreToken, getSubscriberToken, getPublisherToken } from '../utils/token';

export const InjectRedisStore = (name: string) => Inject(getStoreToken(name));
export const InjectRedisPublisher = (name: string) => Inject(getPublisherToken(name));
export const InjectRedisSubscriber = (name: string) => Inject(getSubscriberToken(name));
