import { Module, DynamicModule } from '@nestjs/common';
import { RedisModuleOptions, RedisStoreOptions, RedisChannelOptions } from './ifaces';
import { RedisCoreModule } from './redis-core.module';
import { createStoreProviders, createChannelProviders } from './factory/providers.factory';

@Module({})
export class RedisModule {

  /**
   *
   * Configure the RedisModule
   *
   * @param ops Connection and extra options for redis clients
   */
  static forRoot(ops?: RedisModuleOptions): DynamicModule {
    return {
      module: RedisModule,
      imports: [
        RedisCoreModule.forRoot(ops)
      ],
    };
  }

  /**
   *
   * Create shareable Stores and Channels (Publisher/Subscriber)
   *
   * @param ops stores and channels
   */
  static forFeature(ops: {
    stores?: RedisStoreOptions[],
    channels?: RedisChannelOptions[],
  } = {}): DynamicModule {

    const storeProviders = createStoreProviders(ops.stores);
    const channelProviders = createChannelProviders(ops.channels);

    return {
      module: RedisModule,
      providers: [
        ...storeProviders,
        ...channelProviders
      ],
      exports: [
        ...storeProviders,
        ...channelProviders
      ],
    };
  }

}
