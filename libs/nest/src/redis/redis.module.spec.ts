import { Test, TestingModule } from '@nestjs/testing';
import { Module, Injectable, Global } from "@nestjs/common";
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import { RedisModule } from './redis.module';
import { InjectRedisStore, InjectRedisSubscriber, InjectRedisPublisher } from './decorators/redis.decorators';
import { RedisStore } from './adapters/redis.store';
import { RedisSubscriber } from './adapters/redis.subscriber';
import { RedisPublisher } from './adapters/redis.publisher';

@Injectable()
export class CacheService {

  constructor(
    @InjectRedisStore('cache') public readonly cache: RedisStore,
    @InjectRedisSubscriber('events') public readonly subscriber: RedisSubscriber<string>,
    @InjectRedisPublisher('events') public readonly publisher: RedisPublisher<string>,
  ) {
    // do your magic
  }

}

@Global()
@Module({
  imports: [
    RedisModule.forFeature({
      stores: [ { name: 'cache' }  ],
      channels: [ { name: 'events' }  ],
    })
  ],
  providers: [CacheService],
  exports: [CacheService],
})
export class CacheModule {}

describe('RedisService', () => {

  let service: CacheService;
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      imports: [
        RedisModule.forRoot(),
        CacheModule,
      ],
    }).compile();

    app.useLogger(null);

    service = app.get<CacheService>(CacheService);
  });

  afterAll(async () => {
    await app.close();
    // This is not necesary
    // await client.disconnect();
    // await store.client.disconnect();
  });

  it('should create an usable RedisStore', async () => {
    const store = service.cache;
    expect(typeof store).toBe('object');

    interface Foo { foo: string };

    const item: Foo = { foo: 'bar' };

    const result = await store.set('foo', item);
    expect(result).toBe('OK');

    const fooc = await store.get<Foo>(`foo`);
    expect(fooc).toEqual(item);

    await store.del('foo');

    const exists = await store.exists(`foo`);
    expect(exists).toBe(false);

    await store.set('foo', item, 1);

    await of(1).pipe(delay(1200)).toPromise();

    // Need to be expired
    const eexists = await store.exists(`foo`);
    expect(eexists).toBe(false);

  });

  it('should create an usable RedisSubscriber<T> & RedisPublisher<T>', async (done) => {
    const subscriber = service.subscriber;
    const publisher = service.publisher;
    expect(typeof subscriber).toBe('object');
    expect(publisher.channel).toBe('events');
    expect(typeof publisher).toBe('object');
    expect(publisher.channel).toBe('events');

    const expected = 'bar';
    const sub = subscriber.subscribe(data => {
      expect(data).toEqual(expected);
      done();
      sub.unsubscribe();
    });

    await publisher.publish(expected);
  });

})
