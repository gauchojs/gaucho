import { DynamicModule, Module } from '@nestjs/common';
import { GauchoCoreModule } from './gaucho-core.module';
import { GauchoModuleOptions } from './ifaces/module.options';

@Module({})
export class GauchoModule {

  static forRoot(ops: GauchoModuleOptions): DynamicModule {
    return {
      module: GauchoCoreModule,
      imports: [
        GauchoCoreModule.forRoot(ops)
      ]
    };
  }

}
