import { Module, Global, DynamicModule } from '@nestjs/common';
import { createSession } from './factory';
import { GauchoModuleOptions } from './ifaces/module.options';
import { KeycloakModule } from '@gaucho/nest-keycloak';
import { RepositoryModule } from '../repository';
import { StorageModule } from '../storage';

@Global()
@Module({})
export class GauchoCoreModule {

  static forRoot(ops: GauchoModuleOptions): DynamicModule {

    const imports = [];

    if (ops.auth) {
      const session = createSession(ops.session);
      const aops = { ...ops.auth, session };
      imports.push(KeycloakModule.forRoot(aops));
    }

    if (ops.db) {
      imports.push(RepositoryModule.forRoot(ops.db.uri, ops.db.options));

      if (ops.storage) {
        imports.push(StorageModule.forRoot(ops.storage));
      }
    }

    return {
      module: GauchoCoreModule,
      imports,
    };
  }

}
