import * as expressSession from 'express-session';
import { createRedisClient } from '../../redis';
import { GauchoSessionOptions } from '../ifaces';
import connectRedis from 'connect-redis';

export function createSession(ops: GauchoSessionOptions) {

  const client = createRedisClient(ops.redisUrl, { keyPrefix: ops.keyPrefix });

  const RedisStore = connectRedis(expressSession);

  const sops = {
    secret: ops.secret,
    name: ops.name,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }, // Note that the cookie-parser module is no longer needed
    store:  new RedisStore({ client }),
  };

  return expressSession(sops);
}
