import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import compression from 'compression';
import helmet from 'helmet';
import morgan from 'morgan';
import { GauchoAppOptions } from '../ifaces';
import { createSession } from './session.factory';

export class GauchoFactory {
  public static async create(module: any, ops: GauchoAppOptions): Promise<NestExpressApplication> {

    const app = await NestFactory.create<NestExpressApplication>(module);

    const env = ops.env || 'development';

    if (ops.apiPrefix) {
      app.setGlobalPrefix(ops.apiPrefix);
    }

    //
    //  compression
    app.use(compression());

    //
    // security
    app.use(helmet());

    // CORS
    app.enableCors();

    // Deprecated
    // increment the forms body
    // app.use(bodyParser.json({limit: '1mb'}));
    // app.use(bodyParser.urlencoded({limit: '1mb', extended: true}));

    // is dev?
    if (['test', 'production'].indexOf(env) === -1) {
      app.use(morgan('dev'));
    }

    // is prod?
    if (env === 'production') {
      app.set('trust proxy', 1);
    }

    const session = createSession(ops.session);

    // Session
    app.use(session);

    return app;
  }
}
