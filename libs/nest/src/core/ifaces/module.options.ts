import { MongooseModuleOptions } from '@nestjs/mongoose';
import { KeycloakOptions } from '@gaucho/nest-keycloak';
import { GauchoSessionOptions } from './session.options';
import { StorageModuleOptions } from '../../storage';

export interface GauchoModuleOptions {
  db?: {
    uri: string;
    options?: MongooseModuleOptions;
  },
  auth?: KeycloakOptions;
  storage?: StorageModuleOptions;
  session?: GauchoSessionOptions;
}
