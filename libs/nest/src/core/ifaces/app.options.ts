import { KeycloakOptions } from '@gaucho/nest-keycloak';
import type { EnvironmentType } from '@gaucho/shared';
import { GauchoSessionOptions } from './session.options';

export interface GauchoAppOptions {
  session: GauchoSessionOptions;
  apiPrefix?: string;
  env?: EnvironmentType;
}
