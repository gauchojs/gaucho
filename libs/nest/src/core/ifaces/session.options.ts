export interface GauchoSessionOptions {
  secret: string;
  redisUrl: string;
  keyPrefix: string;
  name: string;
}
