import { Test, TestingModule } from '@nestjs/testing';
import { Module, Injectable } from "@nestjs/common";
import { StorageModule } from "./storage.module";
import { StorageModuleOptions, StorageOptions } from './ifaces';
import { StorageService, StorageTemporalService } from './services';
import { Client } from 'minio';
import { InjectBucket, InjectBucketStorage } from './decorators';
import * as path from 'path';
import { Bucket, BucketStorage } from './adapers';
import { RepositoryModule } from '../repository';
import { BufferedFile, Entity, StorageFile, ImageBreakpoint } from '@gaucho/shared';
import * as fs from 'fs-extra';
import * as mime from 'mime';
import { MongoClient } from 'mongodb';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { StorageCoreModule } from './storage-core.module';

const mongoUri = 'mongodb://localhost:27017/';
const dbName = 'gaucho-test';

const modOps: StorageModuleOptions = {
  extrenalUrl: 'http://locahost:9000/',
};

const clientOps: StorageOptions = {
  endPoint: 'localhost',
  port: 9001,
  accessKey: 'minio',
  secretKey: 'minio123',
  useSSL: false,
};

const BUCKET_NAME = 'gaucho';

const txtFile = path.join(__dirname, 'test/lorem.txt');

@Injectable()
class FilesService {
  constructor(
    @InjectBucket(BUCKET_NAME) public bucket: Bucket,
    @InjectBucketStorage(BUCKET_NAME) public bstorage: BucketStorage,
  ) {}
}

@Module({
  imports: [
    StorageModule.forFeature({
      buckets: [{ name: 'gaucho'}],
    })
  ],
  providers: [FilesService],
  exports: [FilesService],
})
class FilesModule {  }

describe('StorageModule', () => {

  let client: Client;
  let storageService: StorageService;
  let tempService: StorageTemporalService;
  let service: FilesService;
  let app: TestingModule;
  let file: BufferedFile;

  let rootFile: Entity<StorageFile>;
  let relativeFile: Entity<StorageFile>;
  let imageFile: Entity<StorageFile>;

  beforeAll(async () => {
    client = new Client(clientOps);
    app = await Test.createTestingModule({
      imports: [
        RepositoryModule.forRoot(mongoUri + dbName),
        StorageModule.forRoot(modOps, clientOps),
        FilesModule
      ],
    }).compile();

    app.useLogger(null);

    const sdm: StorageCoreModule = app.get<StorageCoreModule>(StorageCoreModule);
    // onModuleInit hooks
    await sdm.onModuleInit();

    storageService = app.get<StorageService>(StorageService);
    tempService = app.get<StorageTemporalService>(StorageTemporalService);

    const source = path.resolve(__dirname, './test/tiger.jpg');
    const buffer = await fs.readFile(source);
    const stat = await fs.stat(source);
    const ext = path.extname(source);
    // Imitate a uploaded file
    file = {
      fieldname: 'file',
      originalname: 'tiger.jpg',
      encoding: '7bit',
      mimetype: mime.getType(ext),
      size: stat.size,
      buffer,
    };
  });

  afterAll(async (done) => {
    await app.close();
    // await dropCollections('storages');
    // await client.removeBucket(BUCKET_NAME);
    done();
  });

  it('should create an StorageService', async () => {
    expect(typeof storageService).toBe('object');
  });

  it('should create an StorageTemporalService', async () => {
    expect(typeof tempService).toBe('object');
  });

  it(`should create an bucket called ${BUCKET_NAME}`, async () => {
    const exists = await client.bucketExists(BUCKET_NAME);
    expect(exists).toBe(true);
  });

  it(`should can use a bucket Injection`, async () => {
    service = app.get<FilesService>(FilesService);
    expect(typeof service).toBe('object');
    expect(typeof service.bucket).toBe('object');
  });

  it(`should can use a bucket storage Injection`, async () => {
    service = app.get<FilesService>(FilesService);
    expect(typeof service).toBe('object');
    expect(typeof service.bstorage).toBe('object');
  });

  it(`should can upload a file in a bucket`, async (done) => {
    const filename = 'lorem.txt';
    service = app.get<FilesService>(FilesService);
    await service.bucket.putObject(filename, txtFile);
    const content = await service.bucket.getObject('lorem.txt');
    expect(typeof content).toBe('object');
    done();
  });

  it(`should can remove a file in a bucket`, async (done) => {
    const filename = 'lorem.txt';
    service = app.get<FilesService>(FilesService);
    await service.bucket.removeObject(filename);
    done();
  });

  it(`should can upload a file in a bucket storage`, async (done) => {
    service = app.get<FilesService>(FilesService);
    rootFile = await service.bstorage.put(file);
    const content = await service.bucket.getObject(rootFile.attributes.filename);
    expect(typeof content).toBe('object');
    done();
  });

  it(`should can upload a file in a bucket storage with relative path`, async (done) => {
    service = app.get<FilesService>(FilesService);
    relativeFile = await service.bstorage.put(file, 'foo/bar');
    const content = await service.bucket.getObject(path.join(relativeFile.attributes.relative, relativeFile.attributes.filename));
    expect(typeof content).toBe('object');
    done();
  });

  it(`should can delete a file in a bucket storage`, async (done) => {
    service = app.get<FilesService>(FilesService);
    await service.bstorage.delete(rootFile.id);
    done();
  });

  it(`should can delete a file in a bucket storage with relative path`, async (done) => {
    service = app.get<FilesService>(FilesService);
    await service.bstorage.deleteByFilename(relativeFile.attributes.filename);
    done();
  });

  it(`should can upload an image in a bucket storage`, async (done) => {
    service = app.get<FilesService>(FilesService);
    const breakpoints: ImageBreakpoint[] = ['avatar', 'thumbnail', 'xs', 'sm', 'md', 'lg', 'xl'];
    imageFile = await service.bstorage.putImage(file, { breakpoints });
    expect(imageFile.attributes.sources.length).toBe(breakpoints.length);
    // console.log(target)
    done();
  });

  it(`should can delete an image in a bucket storage`, async (done) => {
    service = app.get<FilesService>(FilesService);
    await service.bstorage.delete(imageFile.id);
    done();
  });

  it(`should can upload a file in the temporal bucket`, async (done) => {
    const res = await tempService.bucket.put(file);
    expect(typeof res).toBe('object');
    done();
  });

})

// async function dropCollections(...collections: string[]): Promise<void> {

//   const db = await MongoClient.connect(mongoUri, { useUnifiedTopology: true });
//   const dbo = db.db(dbName);

//   if (!dbo) Promise.resolve();

//   await from(collections).pipe(
//     concatMap(async collection => {
//       const col = dbo.collection(collection);
//       if (!col) return Promise.resolve();
//       try {
//         await col.drop();
//       } catch (error) {
//       }
//       return Promise.resolve();
//     })
//   ).toPromise();

//   db.close();
// }
