import { Injectable } from '@nestjs/common';
import { StorageOptions } from '../ifaces'
import { Client } from 'minio';

@Injectable()
export class StorageService {

  private clients: Client[] = [];

  constructor(private ops: StorageOptions) { }

  private checkGlobal() {
    if (!this.ops) {
      throw new Error(`The global StorageOptions is not defined, use StorageModule.forRoot(ops)`);
    }
  }

  /**
   *
   * Create a new MinIO Client
   *
   */
  createClient(options: StorageOptions = this.ops): Client {

    this.checkGlobal();

    const client = new Client(options);

    this.clients.push(client);

    return client;
  }

  // listBuckets(): Promise<BucketItemFromList[]>;

}
