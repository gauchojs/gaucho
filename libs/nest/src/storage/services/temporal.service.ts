import { Injectable, Logger } from '@nestjs/common';
import { StorageModuleOptions, StorageOptions } from '../ifaces'
import { Client } from 'minio';
import { StorageRepository } from './storage.repository';
import { Bucket, BucketStorage } from '../adapers';

@Injectable()
export class StorageTemporalService {

  private client: Client;
  bucket: BucketStorage;

  constructor(
    private modOps: StorageModuleOptions,
    private clientOps: StorageOptions,
    private repo: StorageRepository,
  ) {
    this.checkGlobal();
  }

  private checkGlobal() {
    if (!this.modOps) {
      throw new Error(`The global StorageModuleOptions is not defined, use StorageModule.forRoot(ops)`);
    }
  }

  async initialize(): Promise<void>
  {

    this.client = new Client(this.clientOps);
    const bucket = new Bucket(
      this.client,
      {
        name: this.modOps.temporalBucket,
        region: this.modOps.temporalBucketRegion
      }
    );

    await bucket.check();

    this.bucket = new BucketStorage(this.clientOps, bucket, this.repo);

    Logger.verbose(`StorageTemporalService initialized as ${this.modOps.temporalBucket}`);
  }

}
