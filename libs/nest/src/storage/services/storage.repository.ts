import { ObjectId } from 'mongodb';
import { Injectable, Optional } from "@nestjs/common";
import { StorageFile, Entity } from '@gaucho/shared';
import { StorageModuleOptions } from '../ifaces';
import { Repository, RepositoryRegistry, ModelOptions } from '../../repository';
import { storageSchemaFactory } from '../schema';

@Injectable()
export class StorageRepository {

  private repo: Repository<StorageFile>;

  constructor(ops: StorageModuleOptions, @Optional() registry: RepositoryRegistry) {

    if (!registry) {
      throw new Error(`The RepositoryModule is not imported, use RepositoryModule.forRoot(ops) in the main module`);
    }

    const model: ModelOptions = {
      schema: storageSchemaFactory(ops.extrenalUrl),
      name: ops.collection,
    };

    this.repo = registry.create(model);
  }

  create(data: Partial<StorageFile>, _id?: ObjectId): Promise<Entity<StorageFile>> {
    return this.repo.create(data, _id);
  }

  findById(id: string): Promise<Entity<StorageFile>> {
    return this.repo.findById(id);
  }

  findByFilename(filename: string): Promise<Entity<StorageFile>> {
    return this.repo.findOne({ filename });
  }

  delete(id: string): Promise<Entity<StorageFile>> {
    return this.repo.delete(id);
  }

  deleteByFilename(filename: string): Promise<Entity<StorageFile>> {
    return this.repo.deleteOne({ filename });
  }

}
