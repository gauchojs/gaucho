import { Provider } from '@nestjs/common';
import { BucketOptions, StorageOptions } from '../ifaces';
import { getBucketToken, getBucketStorageToken } from '../utils';
import { Bucket, BucketStorage } from '../adapers';
import { StorageService, StorageRepository } from '../services';

export function createBucketsProviders(options: BucketOptions[] = []): Provider[] {
  const providers: Provider[] = [];

  (options || []).forEach(ops => {
    const bToken = getBucketToken(ops.name);
    const sbToken = getBucketStorageToken(ops.name);

    // The bucket
    providers.push({
      provide: bToken,
      useFactory: async (service: StorageService): Promise<Bucket> => {
        const client = service.createClient();
        const bucket = new Bucket(client, ops);

        await bucket.check();

        return bucket;
      },
      inject: [StorageService]
    });

    // The storage bucket
    providers.push({
      provide: sbToken,
      useFactory: (sops: StorageOptions, bucket: Bucket, repo: StorageRepository): BucketStorage => {
        return new BucketStorage(sops, bucket, repo);
      },
      inject: [StorageOptions, bToken, StorageRepository]
    });

  });

  return providers;
}
