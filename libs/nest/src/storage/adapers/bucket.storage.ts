import { Types } from 'mongoose';
import { ObjectId } from 'mongodb';
import { StorageOptions, StorageFilename, StorageImageOption } from '../ifaces';
import { Bucket } from './bucket';
import { BufferedFile, StorageFile, Entity } from '@gaucho/shared';
import { StorageRepository } from '../services';
import * as mime from 'mime';
import * as path from 'path';
import { optimize } from '@gaucho/media';
import { switchMap, toArray, concatMap } from 'rxjs/operators';

interface PreparedStorageFile {
  _id: ObjectId;
  objectName: string;
  attributes: StorageFile;
  factory: StorageFilename;
}

export class BucketStorage {

  constructor(
    private ops: StorageOptions,
    private bucket: Bucket,
    private repository: StorageRepository,
  ) { }

  async put(file: BufferedFile, relativePath?: string): Promise<Entity<StorageFile>> {
    const target = this.prepare(file, relativePath);

    await this.bucket.putObject(target.objectName, file.buffer);

    return this.repository.create(target.attributes, target._id);
  }

  async putImage(file: BufferedFile, options: StorageImageOption, relativePath?: string): Promise<Entity<StorageFile>> {
    const target = this.prepare(file, relativePath);
    const { factory } = target;

    // put the original
    await this.bucket.putObject(target.objectName, file.buffer);

    return optimize(
      file,
      options.breakpoints,
      options.exportType,
      options.quality
    ).pipe(
      concatMap(async item => {
        const filename = `${item.breakpoint}.${item.exportType}`;
        const objectName = path.join(target._id.toHexString(), filename);
        const size = item.buffer.length;
        await this.bucket.putObject(objectName, item.buffer);
        return { filename, size, breakpoint: item.breakpoint };
      }),
      toArray(),
      switchMap(sources => {
        target.attributes.sources = sources;
        return this.repository.create(target.attributes, target._id);
      }),
    ).toPromise();

  }

  async delete(id: string): Promise<Entity<StorageFile>> {
    const target = await this.repository.findById(id);

    const entries = [
      path.join(target.attributes.relative, target.attributes.filename),
      ...target.attributes.sources.map(entry => path.join(target.attributes.relative, `${target.id}/${entry.filename}`),)
    ]

    await this.bucket.removeObjects(entries);

    return this.repository.delete(id);
  }

  async deleteByFilename(filename: string): Promise<Entity<StorageFile>> {
    const target = await this.repository.findByFilename(filename);

    const entries = [
      path.join(target.attributes.relative, target.attributes.filename),
      ...target.attributes.sources.map(entry => path.join(target.attributes.relative, `${target.id}/${entry.filename}`),)
    ]

    await this.bucket.removeObjects(entries);

    return this.repository.deleteByFilename(filename);
  }

  private prepare(file: BufferedFile, relativePath?: string, temporal = false): PreparedStorageFile {
    const relative = this.normalize(relativePath);
    const _id: ObjectId = Types.ObjectId();
    const ext = mime.getExtension(file.mimetype);
    const filename = `${_id}.${ext}`;
    const factory = new StorageFilename(ext, _id.toHexString());
    const objectName = factory.resolve(relative);

    const attributes: StorageFile = {
      bucketName: this.bucket.bucketName,
      type: file.mimetype.toLowerCase(),
      originalName: file.originalname,
      filename,
      relative,
      size: file.size,
      sources: [],
      temporal
    };

    return {
      _id,
      objectName,
      attributes,
      factory
    };
  }

  private normalize(relative?: string) {
    if (!relative) return '';

    let res = relative;

    const elash = new RegExp('\/$');
    const sslash = new RegExp('^\/');

    res = res.replace(elash, '');
    res = res.replace(sslash, '');

    return `${res}/`;
  }

}
