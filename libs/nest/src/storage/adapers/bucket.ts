import {
  Client,
  BucketItem,
  BucketStream,
  IncompleteUploadedBucketItem,
  ItemBucketMetadata,
  CopyConditions,
  BucketItemCopy,
  BucketItemStat,
  PostPolicyResult,
  PostPolicy,
  NotificationConfig,
} from 'minio';
import { Readable as ReadableStream } from 'stream';
import { BucketOptions } from '../ifaces';

export class Bucket {

  bucketName: string;
  region: string;

  constructor(private client: Client, private ops: BucketOptions) {
    this.bucketName = this.ops.name;
    this.region = this.ops.region || 'default';
  }

  // makeBucket(bucketName: string, region: Region): Promise<void>;
  // bucketExists(bucketName: string): Promise<boolean>;
  async check(): Promise<void> {
    const exists = await this.client.bucketExists(this.ops.name);

    if (!exists) {
      // TODO: Check bucket security
      await this.client.makeBucket(this.bucketName, this.region);
    }
  }

  // removeBucket(bucketName: string): Promise<void>;
  remove(): Promise<void> {
    return this.client.removeBucket(this.bucketName);
  }

  // // Bucket operations

  listObjects(prefix?: string, recursive?: boolean): BucketStream<BucketItem> {
    return this.client.listObjects(this.bucketName, prefix, recursive);
  }

  listObjectsV2(prefix?: string, recursive?: boolean, startAfter?: string): BucketStream<BucketItem> {
    return this.client.listObjectsV2(this.bucketName, prefix, recursive, startAfter);
  }

  listIncompleteUploads(bucketName: string, prefix?: string, recursive?: boolean): BucketStream<IncompleteUploadedBucketItem> {
    return this.client.listIncompleteUploads(this.bucketName, prefix, recursive);
  }

  // // Object operations
  getObject(objectName: string): Promise<ReadableStream> {
    return this.client.getObject(this.bucketName, objectName);
  }

  getPartialObject(objectName: string, offset: number, length?: number): Promise<ReadableStream> {
    return this.client.getPartialObject(this.bucketName, objectName, offset, length);
  }

  fGetObject(objectName: string, filePath: string): Promise<void> {
    return this.client.fGetObject(this.bucketName, objectName, filePath);
  }

  putObject(objectName: string, stream: ReadableStream|Buffer|string, metaData?: ItemBucketMetadata): Promise<string>;

  putObject(objectName: string, stream: ReadableStream|Buffer|string, size?: number, metaData?: ItemBucketMetadata): Promise<string>;

  putObject(objectName: string, stream: ReadableStream|Buffer|string, size?: number | ItemBucketMetadata, metaData?: ItemBucketMetadata): Promise<string> {
    if (typeof size === 'number') {
      return this.client.putObject(this.bucketName, objectName, stream, size, metaData);
    }
    return this.client.putObject(this.bucketName, objectName, stream, size);
  }

  fPutObject(objectName: string, filePath: string, metaData: ItemBucketMetadata): Promise<string> {
    return this.client.fPutObject(this.bucketName, objectName, filePath, metaData)
  }

  copyObject(objectName: string, sourceObject: string, conditions: CopyConditions): Promise<BucketItemCopy> {
    return this.client.copyObject(this.bucketName, objectName, sourceObject, conditions);
  }

  statObject(objectName: string): Promise<BucketItemStat> {
    return this.client.statObject(this.bucketName, objectName);
  }

  removeObject(objectName: string): Promise<void> {
    return this.client.removeObject(this.bucketName, objectName);
  }

  removeObjects(objectsList: string[]): Promise<void> {
    return this.client.removeObjects(this.bucketName, objectsList);
  }

  removeIncompleteUpload(objectName: string): Promise<void> {
    return this.client.removeIncompleteUpload(this.bucketName, objectName);
  }

  // // Presigned operations
  presignedUrl(httpMethod: string, objectName: string, expiry?: number, reqParams?: { [key: string]: any; }, requestDate?: Date): Promise<string> {
    return this.client.presignedUrl(httpMethod, this.bucketName, objectName, expiry, reqParams, requestDate);
  }

  presignedGetObject(objectName: string, expiry?: number, respHeaders?: { [key: string]: any; }, requestDate?: Date): Promise<string> {
    return this.client.presignedGetObject(this.bucketName, objectName, expiry, respHeaders, requestDate);
  }

  presignedPutObject(objectName: string, expiry?: number): Promise<string> {
    return this.client.presignedPutObject(this.bucketName, objectName, expiry);
  }

  presignedPostPolicy(policy: PostPolicy): Promise<PostPolicyResult> {
    return this.client.presignedPostPolicy(policy);
  }

  // // Bucket Policy & Notification operations
  getBucketNotification(): Promise<NotificationConfig> {
    return this.client.getBucketNotification(this.bucketName);
  }

  setBucketNotification(bucketNotificationConfig: NotificationConfig): Promise<void> {
    return this.client.setBucketNotification(this.bucketName, bucketNotificationConfig);
  }

  removeAllBucketNotification(): Promise<void> {
    return this.client.removeAllBucketNotification(this.bucketName);
  }

  // // todo #low Specify events
  // listenBucketNotification(bucketName: string, prefix: string, suffix: string, events: string[]): EventEmitter;

  // getBucketPolicy(bucketName: string): Promise<string>;

  // setBucketPolicy(bucketName: string, bucketPolicy: string): Promise<void>;

  // // Other
  // newPostPolicy(): PostPolicy;
  // setRequestOptions(otpions: AgentOptions): void;

}
