
export function getBucketToken(name: string) {
  return `${name}Bucket`;
}

export function getBucketStorageToken(name: string) {
  return `${name}StorageBucket`;
}
