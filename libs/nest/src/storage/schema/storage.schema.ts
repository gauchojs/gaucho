import { Schema } from 'mongoose';
import { StorageSource } from '@gaucho/shared';

export function storageSchemaFactory(baseUrl: string): Schema {

  const endWithSlash = new RegExp('\/$');
  const url = endWithSlash.test(baseUrl) ? baseUrl : baseUrl + '/';

  const StorageSchema = new Schema({
    type: { type: String, required: true },
    originalName: { type: String },
    bucketName: { type: String },
    filename: { type: String, required: true },
    relative: { type: String },
    size:  { type: Number, required: true },
    sources: [{
      filename: { type: String, required: true },
      size:  { type: Number, required: true },
      breakpoint:  { type: String },
    }],
    temporal: { type: Boolean, required: true, default: false },
    expiration: Date
  });

  StorageSchema.virtual('url').get(function() {
    return `${url}${this.bucketName}/${this.relative}${this.filename}`;
  });

  StorageSchema.virtual('sourcesUrls').get(function() {
    const res: StorageSource[] = this.sources || [];
    return res.map(entry => `${url}${this.bucketName}/${this.relative}${this.id}/${entry.filename}`);
  });

  StorageSchema.virtual('totalSize').get(function() {
    const res: StorageSource[] = this.sources || [];
    if (res.length === 0) return this.size;
    return res.reduce((accumulator, entry) => accumulator + entry.size, this.size);
  });

  StorageSchema.index({ filename: 1 });

  return StorageSchema;
}
