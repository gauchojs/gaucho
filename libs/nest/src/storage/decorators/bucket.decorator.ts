import { Inject } from '@nestjs/common';
import { getBucketToken, getBucketStorageToken } from '../utils';

export const InjectBucket = (bucketName: string) => Inject(getBucketToken(bucketName));
export const InjectBucketStorage = (bucketName: string) => Inject(getBucketStorageToken(bucketName));
