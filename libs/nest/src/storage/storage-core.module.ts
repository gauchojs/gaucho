import { Module, Global, DynamicModule, OnModuleInit } from '@nestjs/common';
import { StorageModuleOptions, StorageOptions } from './ifaces';
import { StorageService, StorageRepository, StorageTemporalService } from './services';

@Global()
@Module({
  providers: [
    StorageService,
    StorageRepository,
    StorageTemporalService,
  ],
  exports: [
    StorageService,
    StorageRepository,
    StorageTemporalService,
  ],
})
export class StorageCoreModule implements OnModuleInit {

  constructor(private temp: StorageTemporalService) {}

  private static defaultsModOps: Partial<StorageModuleOptions> = {
    collection: 'storage',
    temporalBucket: 'temporals',
    temporalBucketRegion: 'default',
    expiration: (1000 * 60) * 30, // 30 minutes
  };

  private static defaultsClientOps: Partial<StorageOptions> = {
    endPoint: 'localhost',
    useSSL: false,
    port: 9000,
    region: 'default',
  };

  static forRoot(moduleOps: StorageModuleOptions, clientOps?: StorageOptions): DynamicModule {
    const modops = { provide: StorageModuleOptions, useValue:  {...this.defaultsModOps, ...moduleOps} };
    const clientops = { provide: StorageOptions, useValue:  {...this.defaultsClientOps, ...clientOps} };
    return {
      global: true,
      module: StorageCoreModule,
      providers: [
        modops,
        clientops,
      ],
      exports: [
        modops,
        clientops,
      ],
    };
  }

  async onModuleInit(): Promise<void> {
    await this.temp.initialize();
    console.log('StorageCoreModule initialized')
  }

};
