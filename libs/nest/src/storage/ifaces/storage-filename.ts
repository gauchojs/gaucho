import { v4 as uuidv4 } from 'uuid';
import * as path from 'path';

function generateHash(deph: number): string {

  if(deph < 1 && deph > 5 ) {
    throw new Error('Invalid deph: deph >= 1 && deph <= 5 ');
  }

  const uuid: string = uuidv4();
  const arr = uuid.split('-');
  arr.splice(0, 5-deph);
  return arr.join('-');
}

export class StorageFilename extends String {

  /**
   *
   * Create a defined StorageFilename
   *
   * NOTE: Not supported filename with only extension like .htaccess
   *
   * @param extname file extension
   * @param filename filename
   */
  constructor(extname: string, filename?: string);

  /**
   *
   * Create a random StorageFilename with uuidv4
   *
   * uuid: `9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d`
   * deph: |   5    | 4  | 3  |  2  |     1     |
   *
   * NOTE: Not supported filename with only extension like .htaccess
   *
   * @param extname file extension
   * @param deph Hash deph >= 1 && deph <= 5, default 1
   *
   * @see https://www.npmjs.com/package/uuid#quickstart
   */
  // tslint:disable-next-line: unified-signatures
  constructor(extname: string, deph?: number);

  constructor(extname: string = '', filename?: string | number) {
    let deph = 1;
    if (typeof filename === 'number') {
      deph = filename;
      filename = null;
    }

    filename = filename ? filename : generateHash(deph);
    const ext = extname.replace(/\.+/, '').trim().toLowerCase();
    const fname = ext ? `${filename}.${ext}` : `${filename}`;
    super(fname);
  }

  /**
   *
   * Export the filname to a prfixed breakpoint like sm@some.txt
   *
   * @param breakpoint prefix
   */
  toPrefix(prefix: string): string {
    return `${prefix}@${this}`;
  }

  /**
   * Get the extension name
   */
  extname(): string {
    return path.extname(this.toString());
  }

  /**
   *
   * Export with a path.join
   *
   * @param relative
   * @param prefix
   */
  resolve(relative: string, prefix?: string): string {
    if (prefix) {
      return path.join(relative, this.toPrefix(prefix));
    }
    return path.join(relative, this.toString());
  }

}
