import { ClientOptions, Region} from 'minio';

export class StorageOptions implements ClientOptions {
  endPoint: string;
  accessKey: string;
  secretKey: string;
  useSSL?: boolean;
  port?: number;
  region?: Region;
  transport?: any;
  sessionToken?: string;
  partSize?: number;
}

export class StorageModuleOptions {
  // MongoDB
  collection?: string;
  extrenalUrl: string;
  // temporalBucket
  temporalBucket?: string;
  temporalBucketRegion?: string;
  expiration?: number;
}
