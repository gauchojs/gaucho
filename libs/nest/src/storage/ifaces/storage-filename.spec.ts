import * as path from 'path';
import * as fs from 'fs-extra';
import { StorageFilename } from './storage-filename';

describe('StorageFilename', () => {

  it(`should handle a constructor`, async () => {
    const filename = new StorageFilename('txt');
    expect(typeof filename).toBe('object');
  })

  it(`should handle a filename`, async () => {
    const filename = new StorageFilename('txt', 'some');
    expect(filename.toString()).toBe('some.txt');
  })

  it(`should handle a hash`, async () => {
    const filename = new StorageFilename('txt');
    expect(filename.toString().length > 4).toBe(true);
  })

  it(`should handle a hash deph`, async () => {
    for (let deph = 1; deph <= 5; deph++) {
      const fd = new StorageFilename('txt', deph);
      expect(fd.toString().split('-').length).toBe(deph);
    }
    const filename = new StorageFilename('txt');
    expect(filename.toString().length > 4).toBe(true);
  })

  it(`should handle a prefix`, async () => {
    const filename = new StorageFilename('txt', 'some');
    expect(filename.toPrefix('xs').toString()).toBe('xs@some.txt');
  })

  it(`should handle a extname`, async () => {
    const filename = new StorageFilename('txt', 'some');
    expect(filename.extname()).toBe('.txt');
    const filename2 = new StorageFilename('.txt', 'some');
    expect(filename2.extname()).toBe('.txt');
    const filename3 = new StorageFilename('.TxT', 'some');
    expect(filename3.extname()).toBe('.txt');
  })

  it(`should handle a resolve`, async () => {
    const filename = new StorageFilename('txt', 'some');
    expect(filename.resolve('home/data')).toBe('home/data/some.txt');
    expect(filename.resolve('home/data/')).toBe('home/data/some.txt');
  })

})
