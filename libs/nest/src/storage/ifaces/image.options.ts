import { ImageBreakpoint } from '@gaucho/shared';
import { ImageType } from '@gaucho/media';

export interface StorageImageOption {
  breakpoints: ImageBreakpoint[],
  exportType?: ImageType,
  quality?: number,
}
