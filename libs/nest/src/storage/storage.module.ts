import { Module, DynamicModule } from '@nestjs/common';
import { StorageOptions, BucketOptions, StorageModuleOptions } from './ifaces';
import { StorageCoreModule } from './storage-core.module';
import { createBucketsProviders } from './factory/providers.factory';

@Module({
  controllers: [],
  providers: [],
})
export class StorageModule {

  /**
   *
   * Configure the StorageModule
   *
   * TODO: Add support for multiple nodes
   *
   * @param moduleOps Storage Module options
   * @param clientOps Connection and extra options for minio clients
   */
  static forRoot(moduleOps: StorageModuleOptions, clientOps?: StorageOptions): DynamicModule {
    return {
      module: StorageModule,
      imports: [
        StorageCoreModule.forRoot(moduleOps, clientOps)
      ],
    };
  }

  /**
   *
   * Create shareable Buckets
   *
   * TODO: Add support for multiple nodes
   *
   * @param ops buckets and channels
   */
  static forFeature(ops: {
    buckets?: BucketOptions[],
  } = {}): DynamicModule {

    const bucketsProviders = createBucketsProviders(ops.buckets);

    return {
      module: StorageModule,
      providers: [
        ...bucketsProviders
      ],
      exports: [
        ...bucketsProviders
      ],
    };
  }

};
