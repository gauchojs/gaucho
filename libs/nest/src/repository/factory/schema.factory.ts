import { ModelOptions } from '../ifaces';
import { omit, has, get } from 'lodash';

export function configureSchema(model: ModelOptions, type: string) {
  const { schema, hidden } = model;

  const rel = Object.keys(schema.obj).filter(key => has(schema.obj[key], 'ref'));
  const relations = rel.map(prop => {
    return {
      attribute: prop,
      type: get(schema.obj[prop], 'ref'),
    };
  });

  schema.set('toObject', { virtuals: true, transform: transform(type, relations, hidden) });
  schema.set('toJSON', { virtuals: true });
}

export const transform = (type: string, relations = [], hide: string[] = []): (doc, ret, options) => any => {
  return function (doc, ret, options) {
    const {id, createdAt, updatedAt } = ret;
    const rels = relations.map(rel => rel.attribute);
    const toHide = ['_id', 'createdAt', 'updatedAt', 'id', '__v', ...hide, ...rels];

    let relationships: any;

    if (relations.length) {
      relationships = {};
      relations.forEach(rel => {
        const obj = ret[rel.attribute];
        if (obj) {
          let aid = typeof obj === 'object' ? obj['id'] : obj.toString();
          if (aid instanceof Buffer) {
            aid = obj.toString();
          }
          relationships[rel.attribute] = { type: rel.type, id: aid };
        }
      });
    }

    const res: any = {
      type,
      id,
      createdAt,
      updatedAt,
      attributes: omit(ret, toHide)
    };

    if (relationships) {
      res.relationships = relationships;
    }

    return res;
  }
}
