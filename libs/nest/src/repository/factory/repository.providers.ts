import { Connection } from 'mongoose';
import { getConnectionToken } from '@nestjs/mongoose';
import { RepositoryRegistry } from '../services/registry.service';
import { getRepositoryToken } from '../utils';
import { ModelOptions } from '../ifaces';

export function createRepositoryProviders(
  connectionName?: string,
  models: ModelOptions[] = [],
) {
  const providers = (models || []).map(model => ({
    provide: getRepositoryToken(model.name),
    useFactory: async (connection: Connection, registry: RepositoryRegistry) => {
      return registry.create(model, connection);
    },
    inject: [getConnectionToken(connectionName), RepositoryRegistry],
  }));
  return [...providers];
}
