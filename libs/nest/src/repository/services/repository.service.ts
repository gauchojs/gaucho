import { Injectable } from '@nestjs/common';
import { RepositoryRegistry } from './registry.service';
import { CollectionResult, Query, IncludeQuery, Entity, SingleResult, SingleQuery } from '@gaucho/shared';
import { from, of } from 'rxjs';
import { concatMap, switchMap, tap } from 'rxjs/operators';
import { groupBy, uniq } from 'lodash';

@Injectable({})
export class RepositoryService {

  constructor(private readonly registry: RepositoryRegistry) { }

  /**
   *
   * Find and paginate (optinal) Document into the repositories and it's the relashionships included
   *
   * @param query The Documents finded/paginated
   * @param includes The includes (relashionship)
   */
  async find<T>(main: Query, ...includes: IncludeQuery[]): Promise<CollectionResult<T>> {
    const now = Date.now();

    const repo = this.registry.get(main.type);

    let docs: Entity<any>[];

    if (main.size > 0 && main.page > 0) {
      docs = await repo.paginate(main.filter, main.page, main.size, main.sort, main.populate, main.select);
    } else {
      docs = await repo.find(main.filter, main.sort, main.populate, main.select);
    }
    const count = await repo.countDocuments(main.filter);

    const toInclude = [];
    await from(includes).pipe(
      concatMap(incl => {
        const {relationship} = incl;

        if (relationship) {
          const relationships = docs.map(doc => doc.relationships[relationship]).filter(rel => rel !== undefined);
          const grouped = groupBy(relationships, 'type');

          return from(Object.keys(grouped)).pipe(
            switchMap(type => {
              const rs = grouped[type];
              const q = { _id: { $in: uniq(rs.map(r => r.id)) } };
              const irepo = this.registry.get(type);

              return irepo.find(q, null, incl.populate, incl.select);
            })
          )
        }

        return null;
      }),
      tap(items => items && toInclude.push(...items)),
    ).toPromise();

    const res = new CollectionResult<T>(docs, Date.now() - now, main.page, main.size, count);

    if (toInclude.length) {
      res.appendInclude(...toInclude);
    }

    return res;
  }

  /**
   *
   * Find an Document in the repositories and it's the relashionships included
   *
   * @param query The Document finded
   * @param includes The includes (relashionship)
   */
  async findById<T>(query: SingleQuery, ...includes: IncludeQuery[]): Promise<SingleResult<T>> {
    const now = Date.now();

    const repo = this.registry.get(query.type);

    const doc = await repo.findById(query.id, query.select, query.populate);

    const toInclude = [];
    await from(includes).pipe(
      concatMap(incl => {
        const {relationship} = incl;

        if (relationship) {
          const rel = doc.relationships[relationship];
          if (rel) {
            const irepo = this.registry.get(rel.type);
            return irepo.findById(rel.id);
          }
        }

        return null;
      }),
      tap(item => item && toInclude.push(item)),
    ).toPromise();

    const res = new SingleResult<T>(doc, Date.now() - now);

    if (toInclude.length) {
      res.appendInclude(...toInclude);
    }

    return res;
  }

}
