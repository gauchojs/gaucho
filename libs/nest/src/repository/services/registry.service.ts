import { Connection, Model } from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';
import { Injectable, Optional } from "@nestjs/common";
import { Repository } from '../adapters';
import { ModelOptions } from '../ifaces';
import { configureSchema } from '../factory/schema.factory';

@Injectable({})
export class RepositoryRegistry {

  private repos: { name: string, collection: string, repository:Repository<any> }[] = [];

  constructor(@Optional() @InjectConnection() private conn: Connection) {
  }

  register(name: string, repository: Repository<any>) {
    this.repos.push({name, collection: repository.collectionName, repository});
  }

  configure(model: ModelOptions, connection?: Connection): Model<any> {
    let rmodel: Model<any>;
    model.schema.set('timestamps', true);
    if (!connection) {
      // ! WARN using the default connection
      rmodel = this.conn.model(model.name, model.schema, model.collection);
    } else {
      rmodel = connection.model(model.name, model.schema, model.collection);
    }
    configureSchema(model, rmodel.collection.collectionName);

    return rmodel;
  }

  create<T = any>(model: ModelOptions, connection?: Connection): Repository<T> {
    const rmodel = this.configure(model, connection);
    const repo = new Repository(rmodel, model.defaultSize);
    this.register(model.name, repo);
    return this.get(model.name);
  }

  /**
   * Count the repositories
   */
  count(): number {
    return this.repos.length;
  }

  /**
   *
   * Get an registered repostory
   *
   * @param name model name or collection name
   */
  get<T = any>(name: string): Repository<T> {
    const repo = this.repos.find(r => r.name === name || r.collection === name);

    if (!repo) {
      throw new Error(`Invalid repository name ${name}`)
    }

    return repo.repository;
  }

}
