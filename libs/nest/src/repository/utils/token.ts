
export function getRepositoryToken(model: string) {
  return `${model}Repository`;
}
