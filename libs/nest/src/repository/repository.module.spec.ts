import { Test, TestingModule } from '@nestjs/testing';
import { Module, NotFoundException } from "@nestjs/common";
import { RepositoryModule } from './repository.module';
import { RepositoryRegistry, RepositoryService } from './services';
import { MongoClient } from 'mongodb';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { Schema, SchemaTypes } from 'mongoose';
import { Repository } from './adapters';
import * as fs from 'fs-extra';
import * as path from 'path';
import { random } from 'lodash';
import { Entity, Query } from '@gaucho/shared';

const mongoUri = 'mongodb://localhost:27017/';
const dbName = 'gaucho-test';

interface User {
  username: string,
  password?: string,
  roles?: string[],
}

const UserSchema = new Schema({
  username: String,
  password: String,
  roles: [String],
});

@Module({
  imports: [
    RepositoryModule.forFeature([{
      name: 'user',
      schema: UserSchema,
      hidden: [ 'password' ]
    }])
  ],
})
class UserModule {  }

interface Article {
  title: string,
  category: string[],
  body?: string,
  author?: string,
  icon?: string;
}

const ArticleSchema = new Schema({
  title: String,
  category: [String],
  body: String,
  author: { type: SchemaTypes.ObjectId, ref: 'user' }
});

ArticleSchema.virtual('icon')
  .get(function() {
    return `icon.png`;
  });

ArticleSchema.index({ title: 1 });
ArticleSchema.index({ category: 1 });

@Module({
  imports: [
    RepositoryModule.forFeature([{
      name: 'article',
      schema: ArticleSchema
    }])
  ],
})
class ArticleModule {  }

describe('RepositoryModule', () => {

  let registry: RepositoryRegistry;
  let repoService: RepositoryService;
  let app: TestingModule;
  let anId: string;

  beforeAll(async () => {
    await dropCollections('articles', 'users');
    app = await Test.createTestingModule({
      imports: [
        RepositoryModule.forRoot(mongoUri + dbName),
        UserModule,
        ArticleModule,
      ],
    }).compile();

    app.useLogger(null);

    registry = app.get<RepositoryRegistry>(RepositoryRegistry);
    repoService = app.get<RepositoryService>(RepositoryService);
  });

  afterAll(async () => {
    await app.close();
  });

  it('should create an RepositoryRegistry', async () => {
    expect(typeof registry).toBe('object');
  });

  it('should create an RepositoryService', async () => {
    expect(typeof repoService).toBe('object');
  });

  it('RepositoryRegistry should have two models', async () => {
    expect(registry.count()).toBe(2);
  });

  it('RepositoryRegistry should get repositories', async () => {
    const userByName = registry.get('user');
    expect(typeof userByName).toBe('object');
    const userByCollection = registry.get('users');
    expect(typeof userByCollection).toBe('object');

    const articleByName = registry.get('article');
    expect(typeof articleByName).toBe('object');
    const articleByCollection = registry.get('articles');
    expect(typeof articleByCollection).toBe('object');

    try {
      registry.get('notfound');
    } catch (error) {
      expect(error.message).toEqual('Invalid repository name notfound');
    }
  });

  it('Repository<T> should handle CRUD', async () => {
    const repository: Repository<User> = registry.get('user');

    // CREATE
    const user = await repository.create({ username: 'test', password: '123' });
    expect(typeof user).toBe('object');
    expect(!!user['type']).toBe(true);
    expect(!!user['id']).toBe(true);
    expect(!!user['createdAt']).toBe(true);
    expect(!!user['updatedAt']).toBe(true);
    expect(!!user['attributes']).toBe(true);
    expect(user.createdAt instanceof Date).toBe(true);
    expect(user.updatedAt instanceof Date).toBe(true);
    expect(user.type).toBe('users');
    // hidden
    expect(user.attributes.password).toEqual(undefined);

    // RETRIVE
    const {id} = user;
    const fuser = await repository.findById(id);
    expect(fuser.id).toBe(id);
    expect(fuser.attributes.username).toBe('test');

    // UPDATE
    const uuser = await repository.update(id, { username: 'test1' });
    expect(uuser.id).toBe(id);
    expect(uuser.attributes.username).toBe('test1');

    // DELETE
    const duser = await repository.delete(id);
    expect(duser.id).toBe(id);
    try {
      const item = await repository.findById(id);
      expect(item).toBe(null);
    } catch (error) {
      expect(error instanceof NotFoundException).toBe(true);
    }

  });

  it('Repository<T> should handle find and pagination', async () => {
    const repository: Repository<Article> = registry.get('article');
    await seed(registry);
    // find
    const q = { category: { $in: [ 'Romance' ]  } };
    const romance = await repository.find(q);
    expect(romance.length).toBe(64);

    // pagination
    const fisrt = await repository.paginate(q, 1, 10);
    expect(fisrt instanceof Array).toBe(true);
    expect(fisrt.length).toBe(10);

    const last = await repository.paginate(q, 7, 10);
    expect(last.length).toBe(4);

  })

  it('Repository<T> should handle sort and population', async () => {
    const repository: Repository<Article> = registry.get('articles');

    // find sorted & populated
    const q = { category: { $in: [ 'Mystery' ]  } };
    const result = await repository.find(q, 'title', 'avatar');
    expect(result instanceof Array).toBe(true);
    expect(result.length).toBe(25);
    const first = result[0];
    const second = result[1];

    expect(first.attributes.title).toBe('Abominable Dr. Phibes, The');
    expect(second.attributes.title).toBe('Alien Raiders');
    expect(first.attributes.icon).toBe('icon.png');
    expect(second.attributes.icon).toBe('icon.png');

    const presult = await repository.paginate(q, 1, 10, 'title', 'avatar');
    expect(presult instanceof Array).toBe(true);
    expect(presult.length).toBe(10);
    const pfirst = presult[0];
    const psecond = presult[1];

    expect(pfirst.attributes.title).toBe('Abominable Dr. Phibes, The');
    expect(psecond.attributes.title).toBe('Alien Raiders');
    expect(pfirst.attributes.icon).toBe('icon.png');
    expect(psecond.attributes.icon).toBe('icon.png');

  })

  it('Repository<T> should handle relationships', async () => {
    const repository: Repository<Article> = registry.get('articles');

    // find sorted & populated
    const q = { title: 'Abominable Dr. Phibes, The' };
    const result = await repository.find(q);
    const first = result[0];

    expect(typeof first.relationships['author']).toBe('object');
    expect(typeof first.relationships['author'].type).toBe('string');
    expect(typeof first.relationships['author'].id).toBe('string');
  })

  it('Repository<T> should handle select', async () => {
    const repository: Repository<Article> = registry.get('articles');

    // find sorted & populated
    const q = { title: 'Abominable Dr. Phibes, The' };
    const result = await repository.find(q, null, null, 'title');
    const first = result[0];

    expect(Object.keys(first.attributes).length).toBe(1);
    anId = first.id;
  })


  it('RepositoryService should handle findBy', async () => {
    const result = await repoService.findById({ type: 'articles', id: anId}, { relationship: 'author' });
    expect(typeof result).toBe('object');
    expect(result.data.id).toBe(anId);
    expect(result.included instanceof Array).toBe(true);
    expect(result.included.length).toBe(1);
  })


  it('RepositoryService should handle find & pagination with relationship', async () => {
    const filter = { title: { $in: [ 'Alien Raiders', 'Adult Camp'] } };
    const q: Query = { type: 'articles', filter };
    // single find
    const result = await repoService.find(q, { relationship: 'author' });
    expect(result.meta.took > 0).toBe(true);
    expect(result.meta.count).toBe(2);
    expect(result.data instanceof Array).toBe(true);
    expect(result.data['length']).toBe(2);
    expect(result.included instanceof Array).toBe(true);
    expect(result.included['length']).toBe(2);
    // paginated
    q.size = 10;
    q.page = 1;
    const presult = await repoService.find(q, { relationship: 'author' });
    expect(presult.meta.took > 0).toBe(true);
    expect(presult.meta.count).toBe(2);
    expect(presult.meta.pages).toBe(1);
    expect(presult.meta.page).toBe(1);
    expect(presult.meta.prev).toBe(null);
    expect(presult.meta.next).toBe(null);
    expect(presult.meta.hasNext).toBe(false);
    expect(presult.meta.hasPrev).toBe(false);
    expect(presult.data instanceof Array).toBe(true);
    expect(presult.data['length']).toBe(2);
    expect(presult.included instanceof Array).toBe(true);
    expect(presult.included['length']).toBe(2);

  })

})

async function seed(registry: RepositoryRegistry): Promise<void> {

  const repoArticles = registry.get('articles');
  const repoUsers = registry.get('users');

  await dropCollections('users');

  const rawUsers = await fs.readJSON(path.join(__dirname, './test/users.json'));

  await from(rawUsers).pipe(
    concatMap((item: User) => {
      return from(repoUsers.create(item))
    })
  ).toPromise();

  const ures = await repoUsers.find();
  const users = ures as Entity<User>[];

  const articles = await fs.readJSON(path.join(__dirname, './test/articles.json'));

  await from(articles).pipe(
    concatMap((item: any) => {
      const doc: Article = {
        title: item.title,
        category: item.category.split('|'),
        body: item.body,
      };
      const usr = users[random(0, users.length-1)];
      doc.author = usr.id;
      return from(repoArticles.create(doc))
    })
  ).toPromise();

}

async function dropCollections(...collections: string[]): Promise<void> {

  const db = await MongoClient.connect(mongoUri, { useUnifiedTopology: true });
  const dbo = db.db(dbName);

  if (!dbo) Promise.resolve();

  await from(collections).pipe(
    concatMap(async collection => {
      const col = dbo.collection(collection);
      if (!col) return Promise.resolve();
      try {
        await col.drop();
      } catch (error) {
        console.log(`Error droping collections ${collection}`)
      }
      return Promise.resolve();
    })
  ).toPromise();

  db.close();
}
