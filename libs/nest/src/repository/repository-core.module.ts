import { Global, Module, DynamicModule } from '@nestjs/common';
import { MongooseModuleOptions, MongooseModuleAsyncOptions, MongooseModule } from '@nestjs/mongoose';
import { RepositoryRegistry } from './services';
import { RepositoryService } from './services/repository.service';

@Global()
@Module({
  providers: [RepositoryRegistry, RepositoryService],
  exports: [RepositoryRegistry, RepositoryService],
})
export class RepositoryCoreModule {

  private static defaultsOps = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  };

  static forRoot(uri: string, options: MongooseModuleOptions = {} ): DynamicModule {
    return {
      global: true,
      module: RepositoryCoreModule,
      imports: [
        MongooseModule.forRoot(uri, {...this.defaultsOps, ...options})
      ],
    };
  }

  static forRootAsync(options: MongooseModuleAsyncOptions): DynamicModule {
    return {
      global: true,
      module: RepositoryCoreModule,
      imports: [
        MongooseModule.forRootAsync({...this.defaultsOps, ...options})
      ],
    };
  }

}
