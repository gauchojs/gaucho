import { Module, DynamicModule } from '@nestjs/common';
import { MongooseModuleOptions, MongooseModuleAsyncOptions } from '@nestjs/mongoose';
import { RepositoryCoreModule } from './repository-core.module';
import { createRepositoryProviders } from './factory';
import { ModelOptions } from './ifaces';

@Module({})
export class RepositoryModule {

  static forRoot(uri: string, options: MongooseModuleOptions = {} ): DynamicModule {
    return {
      module: RepositoryModule,
      imports: [
        RepositoryCoreModule.forRoot(uri, options)
      ],
    };
  }

  static forRootAsync(options: MongooseModuleAsyncOptions): DynamicModule {
    return {
      module: RepositoryModule,
      imports: [
        RepositoryCoreModule.forRootAsync(options)
      ],
    };
  }

  static forFeature(
    models: ModelOptions[] = [],
    connectionName?: string,
  ): DynamicModule {

    const repos = createRepositoryProviders(connectionName, models);

    return {
      module: RepositoryModule,
      providers: [...repos],
      exports: [...repos],
    };
  }

}
