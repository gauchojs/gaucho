import { Schema } from 'mongoose';

export interface ModelOptions {

  name: string;
  schema: Schema;
  collection?: string;
  hidden?: string[];
  defaultSize?: number;

}
