// export interface ResourceModel extends IResourceBase, DbEntityBase, Document {}
import { EntityBase } from '@gaucho/shared';
import { Document } from 'mongoose';

export type EntityModel<T> = T & EntityBase & Document;
