import { RepositoryEventType } from './event-type';
import { Entity } from '@gaucho/shared';

export class RespositoryEvent<T> {
  constructor(public target: Entity<T>) { }
}

export class RespositoryCreatedEvent<T> extends RespositoryEvent<T> {
}

export class RespositoryUpdatedEvent<T> extends RespositoryEvent<T> {
}

export class RespositoryDeletedEvent<T> extends RespositoryEvent<T> {
}

export class RespositoryChangedEvent<T> extends RespositoryEvent<T> {
  constructor(public readonly target: Entity<T>, public readonly type: RepositoryEventType ){
    super(target);
  }
}
