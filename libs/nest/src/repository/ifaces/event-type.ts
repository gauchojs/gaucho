export enum RepositoryEventTypeEnum {
  Init = 'init',
  Find = 'find',
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
  DeleteMany = 'deleteMany',
  UpdateMany = 'updateMany',
}

export type RepositoryEventType = RepositoryEventTypeEnum | string;
