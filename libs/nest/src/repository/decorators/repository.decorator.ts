import { Inject } from '@nestjs/common';
import { getRepositoryToken } from '../utils';

export const InjectRespository = (model: string) => Inject(getRepositoryToken(model));
