import { createParamDecorator, ExecutionContext, NotFoundException } from '@nestjs/common';
import { Query } from '@gaucho/shared';
import { get } from 'lodash';

export type RepositoryQueryDecorator = (type: string) => ParameterDecorator;

export const RepositoryQuery: RepositoryQueryDecorator = createParamDecorator( (type: string, ctx: ExecutionContext) => {
  const {query} = ctx.switchToHttp().getRequest();

  if (!type) {
    throw new NotFoundException('Type not found')
  }

  const result: Query = { type };

  // pagination
  result.page = Number(get(query, 'page.number'));
  result.size = Number(get(query, 'page.size'));

  result.filter = get(query, 'filter');

  // sort
  const sort: string[] = get(query, 'sort') || [];
  if (sort.length) {
    result.sort = sort.join(',');
  }

  // select
  const select: string[] = get(query, 'select') || [];
  if (select.length) {
    result.select = select.join(',');
  }

  // populate
  const populate: string[] = get(query, 'populate') || [];
  if (populate.length) {
    result.populate = populate.join(',');
  }

  return result;
});
