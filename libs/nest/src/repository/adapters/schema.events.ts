import { Document, Schema } from 'mongoose';
import { EventEmitter } from 'events';
import { RepositoryEventTypeEnum } from '../ifaces';

export class SchemaEvents extends EventEmitter {

  constructor(private schema: Schema) {
    super();
    this.initialize();
  }

  private initialize() {

    this.schema.pre('save', function (next) {
      // Add this to keep the isNew after first time save
      this['__wasNew'] = this.isNew;
      next();
    });
    this.schema.post('save', (doc: Document) => {
      if (doc['__wasNew']) {
        // dispatch creation
        return this.emit(RepositoryEventTypeEnum.Create, doc);
      }
      // dispatch updates
      this.emit(RepositoryEventTypeEnum.Update, doc);
    });

    this.schema.post('remove', (doc: Document) => {
      // dispatch deletion
      this.emit(RepositoryEventTypeEnum.Delete, doc);
    });

    this.schema.post('find', (docs) => {
      // TODO: Get ids
      this.emit(RepositoryEventTypeEnum.Find, null);
    });

    this.schema.post('deleteMany', (res) => {
      // TODO: Get ids
      this.emit(RepositoryEventTypeEnum.DeleteMany, null);
    });

    this.schema.post('updateMany', (res) => {
      // TODO: Get ids
      this.emit(RepositoryEventTypeEnum.UpdateMany, null);
    });
  }

}
