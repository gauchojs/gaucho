import { Observable, of, BehaviorSubject } from 'rxjs'
import { switchMap, filter } from 'rxjs/operators';
import { Schema } from 'mongoose';
import {
  RespositoryChangedEvent,
  RespositoryCreatedEvent,
  RespositoryUpdatedEvent,
  RespositoryDeletedEvent,
  RepositoryEventTypeEnum
} from '../ifaces';
import { SchemaEvents } from './schema.events';

export class RepositoryNotifier<T = any> {

  private notifier: SchemaEvents;

  private _changes = new BehaviorSubject<RespositoryChangedEvent<T>>(
    new RespositoryChangedEvent(null, RepositoryEventTypeEnum.Init)
  );
  changes$ = this._changes.asObservable();

  created$: Observable<RespositoryCreatedEvent<T>>;
  updated$: Observable<RespositoryUpdatedEvent<T>>;
  removed$: Observable<RespositoryDeletedEvent<T>>;

  constructor(schema: Schema) {
    this.notifier = new SchemaEvents(schema);
    this.initialize();
  }

  private initialize() {

    this.created$ = this.changes$.pipe(
      filter(event => event.type === RepositoryEventTypeEnum.Create),
      switchMap(event => {
        return of(new RespositoryCreatedEvent<T>(event.target));
      })
    );

    this.updated$ = this.changes$.pipe(
      filter(event => event.type === RepositoryEventTypeEnum.Update),
      switchMap(event => {
        return of(new RespositoryUpdatedEvent<T>(event.target));
      })
    );

    this.removed$ = this.changes$.pipe(
      filter(event => event.type === RepositoryEventTypeEnum.Delete),
      switchMap(event => {
        return of(new RespositoryDeletedEvent<T>(event.target));
      })
    );

    this.notifier.on(
      RepositoryEventTypeEnum.Create,
      doc => this._changes.next(new RespositoryChangedEvent(doc, RepositoryEventTypeEnum.Create))
    );
    this.notifier.on(
      RepositoryEventTypeEnum.Update,
      doc => this._changes.next(new RespositoryChangedEvent(doc, RepositoryEventTypeEnum.Update))
    );
    this.notifier.on(
      RepositoryEventTypeEnum.Delete,
      doc => this._changes.next(new RespositoryChangedEvent(doc, RepositoryEventTypeEnum.Delete))
    );

  }

}
