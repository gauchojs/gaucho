import { RepositoryNotifier } from './repository-notifier';
import { EntityModel } from '../ifaces';
import { Model } from 'mongoose';
import { Entity } from '@gaucho/shared';
import { NotFoundException } from '@nestjs/common';
import { FilterQuery } from 'mongoose';
import { ObjectId } from 'mongodb';
import { pick } from 'lodash';

export class Repository<T> {

  events: RepositoryNotifier<T>;

  get collectionName(): string {
    return this.model.collection.collectionName;
  }

  constructor(
    public readonly model: Model<EntityModel<T>>,
    private defaultSize = 10,
    private defaultSort?: string,
    private defaultPopulate?: string,
  ) {
    this.events = new RepositoryNotifier<T>(model.schema);
  }

  /**
   *
   * Count the Documents of the query
   *
   * @param query a mongoose filter object
   */
  async countDocuments(query: FilterQuery<EntityModel<T>> = {}): Promise<number> {
    return this.model.find(query).countDocuments().exec()
  }

  /**
   *
   * Find and paginate Documents by `query`
   *
   * @param query a mongoose filter object
   * @param page current page
   * @param size page size
   * @param sort a mongoose sort string
   * @param populate a mongoose population string
   */
  async paginate(
    query: FilterQuery<EntityModel<T>> = {},
    page = 1,
    size = this.defaultSize,
    sort = this.defaultSort,
    populate = this.defaultPopulate,
    select?: string,
  ): Promise<Entity<T>[]> {
    const totalDocs = await this.countDocuments(query);
    const pages = (size > 0) ? (Math.ceil(totalDocs / size) || 1) : null;

    if (page < 1 || page > pages) {
      return [];
    }

    const skip = (page - 1) * size;

    const qq = this.findQuery(query, sort, populate, select);

    qq.skip(skip).limit(size);

    const docs = (await qq.exec()).map(doc => doc.toObject() as Entity<T>);

    const selects: string[] = this.parseSelect(select);
    if (selects.length > 0) {
      return docs.map(item => ({
        ...item,
        attributes: pick(item.attributes, selects) })
      );
    }

    return docs;
  }

  /**
   *
   * Find Documents by `query`
   *
   * @param query a mongoose filter object
   * @param sort a mongoose sort string
   * @param populate a mongoose population string
   */
  async find(
    query: FilterQuery<EntityModel<T>> = {},
    sort = this.defaultSort,
    populate = this.defaultPopulate,
    select?: string,
  ): Promise<Entity<T>[]> {
    const qq = this.findQuery(query, sort, populate, select);

    const docs = (await qq.exec()).map(doc => doc.toObject() as Entity<T>);

    const selects: string[] = this.parseSelect(select);
    if (selects.length > 0) {
      return docs.map(item => ({ ...item, attributes: pick(item.attributes, selects) }));
    }

    return docs;
  }

  /**
   *
   * Find one document
   *
   * @param attributes the Document attributes
   */
  async findOne(attributes: Partial<T>): Promise<Entity<T>> {
    const conditions: any = {...attributes};
    return (await this.model.findOne(conditions)).toObject()  as Entity<T>;
  }

  /**
   *
   * Create a new Document
   *
   * @param attributes the Document attributes
   * @param _id the Document id
   */
  async create(attributes: Partial<T>, _id?: ObjectId): Promise<Entity<T>> {
    return (await new this.model({ ...attributes, _id }).save()).toObject() as Entity<T>;
  }

  /**
   *
   * Find a document
   *
   * @param id the document id
   */
  async findById(id: string, select?: string, populate = this.defaultPopulate): Promise<Entity<T>> {
    const doc = await this.findOneById(id, populate);

    const selects: string[] = this.parseSelect(select);

    const item = doc.toObject()  as Entity<T>;

    // make sure the selection
    if (selects.length > 0) {
      item.attributes = pick(item.attributes, selects);
    }

    return item;
  }

  /**
   *
   * Find and update a Document
   *
   * @param id  the document id
   * @param attributes  the Document attributes to update
   */
  async update(id: string, attributes: Partial<T>): Promise<Entity<T>> {
    const target = await this.findOneById(id);

    Object.assign(target, attributes);

    return (await target.save()).toObject() as Entity<T>;
  }

  /**
   *
   * Delete a Document
   *
   * @param id the document id
   */
  async delete(id: string): Promise<Entity<T>> {
    const target = await this.findOneById(id);

    return (await target.remove()).toObject();
  }

  /**
   *
   * Delete a Document
   *
   * @param id the document id
   */
  async deleteOne(attributes: Partial<T>): Promise<Entity<T>> {
    const conditions: any = {...attributes};
    const target = await this.model.findOne(conditions);
    return (await target.remove()).toObject();
  }

  private findQuery(
    query: FilterQuery<EntityModel<T>> = {},
    sort = this.defaultSort,
    populate = this.defaultPopulate,
    select?: string
  ) {
    const qq = this.model.find(query);

    const selects: string[] = this.parseSelect(select);

    if (sort) {
      qq.sort(sort);
    }

    if (populate) {
      qq.populate(populate);
    }

    if (selects.length > 0) {
      qq.select(selects);
    }

    return qq;
  }

  private async findOneById(id: string, populate?: string): Promise<EntityModel<T>> {
    const target = await this.model.findById(id).populate(populate);

    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }

  private parseSelect(select: string): string[] {
    let selects: string[] = [];

    if (select) {
      selects = select.split(' ');
        if (selects.length > 0) {
        ['_id', 'id', 'createdAt', 'updatedAt'].forEach(prop => {
          if(selects.indexOf(prop) === -1) {
            selects.push(prop);
          }
        });
      }
    }

    return selects;
  }

}
