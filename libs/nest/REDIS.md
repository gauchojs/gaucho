# @gaucho/nest/redis

## Description

Usefull module to work with redis

## Installation

```bash
$ npm i --save @gaucho/nest ioredis
```

## RedisClient

To create a simple RedisClient the RedisModule does not need to be imported, for example a client for express-session


```ts
import { createRedisClient } from '@gaucho/nest/redis';

const client = createRedisClient('redis://localhost:6379/session');
```

## The service

```ts
import { Module } from '@nestjs/common';
import { RedisModule, RedisClient } from '@gaucho/nest/redis';

@Module({
  imports: [ RedisModule.forRoot({ url: 'redis://localhost:6379' }) ],
  providers: [CacheService]
})
export class MyModule {}

@Injectable()
export class CacheService {

  client: RedisClient;

  constructor(service: RedisService) {
    this.client = service.createClient('cache');
  }

}
```

## The Store

```ts
import { Module, Injectable } from '@nestjs/common';
import { RedisModule, RedisStore } from '@gaucho/nest/redis';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';

interface Foo {
  bar: string;
}

@Module({
  imports: [ RedisModule.forRoot({ url: 'redis://localhost:6379' }) ],
  providers: [CacheService]
})
export class MyModule {}

@Injectable()
export class CacheService {

  store: RedisStore;

  constructor(service: RedisService) {
    this.store = service.createStore('cache', /*expire optional*/3);

    const foo: Foo = { bar: 'some' };
    const expire = 2; // override the expiration

    this.store.set<Foo>('foo', foo, expire);
    const obj = this.store.get<Foo>('foo');
    console.log(obj.bar); // output: some

    this.getFoo();
  }

  await getFoo() {
    // wait 4 seconds
    await of(1).pipe(delay(4000)).toPromise();
    const exists = await store.exists(`foo`);
    console.log(exists) // output: false
  }

}
```

## The Subscriber/Publisher thing

```ts
import { Module } from '@nestjs/common';
import { RedisModule, RedisSubscriber, RedisPublisher } from '@gaucho/nest/redis';
import { filter } from 'rxjs/operators';

interface Foo {
  bar: string;
}

@Module({
  imports: [ RedisModule.forRoot({ url: 'redis://localhost:6379' }) ],
  providers: [CacheService]
})
export class MyModule {}

@Injectable()
export class CacheService {

  subscriber: RedisSubscriber<Foo>; // is a Subject<Foo>
  publisher: RedisPublisher<Foo>;

  constructor(service: RedisService) {
    this.subscriber = service.createSubscriber<Foo>('mychannel');
    this.publisher = service.createPublisher<Foo>('mychannel');

    this.subscriber.pipe(
      filter(data => data.bar === 'some');
    ).subscribe(console.log); // output: { bar: "some" }

    this.publisher.publish({ bar: 'some' });
    this.publisher.publish({ bar: 'other' });
  }

}
```


## Shareable Store, Publisher and subscriber by Injection

```ts
import { Module } from '@nestjs/common';
import { 
  RedisModule,
  InjectRedisStore,
  InjectRedisPublisher,
  InjectRedisSubscriber,
  RedisStore,
  RedisSubscriber,
  RedisPublisher
} from '@gaucho/nest/redis';

@Module({
  imports: [ RedisModule.forRoot({ url: 'redis://localhost:6379' }) ],
  providers: []
})
export class MainModule {}

@Module({
  imports: [
    RedisModule.forFeature({
      stores: [ { name: 'cache' }  ],
      channels: [ { name: 'events', url: 'redis://otherhost' }  ],
    })
  ],
  providers: [CacheService]
})
export class CacheModule {}

@Injectable()
export class CacheService {

  constructor(
    @InjectRedisStore('cache') private readonly cache: RedisStore,
    @InjectRedisSubscriber('events') private readonly subscriber: RedisSubscriber,
    @InjectRedisPublisher('events') private readonly publisher: RedisPublisher,
  ) {
    // do your magic
  }

}
```
