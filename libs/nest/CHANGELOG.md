# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.19](https://gitlab.com/gauchojs/gaucho/compare/v0.0.18...v0.0.19) (2020-09-17)

**Note:** Version bump only for package @gaucho/nest





## [0.0.18](https://gitlab.com/gauchojs/gaucho/compare/v0.0.17...v0.0.18) (2020-09-01)

**Note:** Version bump only for package @gaucho/nest





## [0.0.17](https://gitlab.com/gauchojs/gaucho/compare/v0.0.16...v0.0.17) (2020-09-01)

**Note:** Version bump only for package @gaucho/nest





## [0.0.16](https://gitlab.com/gauchojs/gaucho/compare/v0.0.15...v0.0.16) (2020-09-01)

**Note:** Version bump only for package @gaucho/nest





## [0.0.15](https://gitlab.com/gauchojs/gaucho/compare/v0.0.14...v0.0.15) (2020-08-26)

**Note:** Version bump only for package @gaucho/nest





## [0.0.14](https://gitlab.com/gauchojs/gaucho/compare/v0.0.13...v0.0.14) (2020-06-24)

**Note:** Version bump only for package @gaucho/nest





## [0.0.13](https://gitlab.com/gauchojs/gaucho/compare/v0.0.12...v0.0.13) (2020-06-24)

**Note:** Version bump only for package @gaucho/nest





## [0.0.12](https://gitlab.com/gauchojs/gaucho/compare/v0.0.11...v0.0.12) (2020-06-24)

**Note:** Version bump only for package @gaucho/nest





## [0.0.11](https://gitlab.com/gauchojs/gaucho/compare/v0.0.10...v0.0.11) (2020-06-23)

**Note:** Version bump only for package @gaucho/nest





## [0.0.10](https://gitlab.com/gauchojs/gaucho/compare/v0.0.9...v0.0.10) (2020-06-05)

**Note:** Version bump only for package @gaucho/nest





## [0.0.9](https://gitlab.com/gauchojs/gaucho/compare/v0.0.8...v0.0.9) (2020-06-05)

**Note:** Version bump only for package @gaucho/nest





## [0.0.8](https://gitlab.com/gauchojs/gaucho/compare/v0.0.7...v0.0.8) (2020-06-05)

**Note:** Version bump only for package @gaucho/nest





## [0.0.7](https://gitlab.com/gauchojs/gaucho/compare/v0.0.6...v0.0.7) (2020-06-04)

**Note:** Version bump only for package @gaucho/nest





## [0.0.6](https://gitlab.com/gauchojs/gaucho/compare/v0.0.5...v0.0.6) (2020-06-04)

**Note:** Version bump only for package @gaucho/nest





## [0.0.5](https://gitlab.com/gauchojs/gaucho/compare/v0.0.4...v0.0.5) (2020-06-04)

**Note:** Version bump only for package @gaucho/nest





## [0.0.4](https://gitlab.com/gauchojs/gaucho/compare/v0.0.3...v0.0.4) (2020-06-04)

**Note:** Version bump only for package @gaucho/nest





## [0.0.3](https://gitlab.com/gauchojs/gaucho/compare/v0.0.2...v0.0.3) (2020-06-04)

**Note:** Version bump only for package @gaucho/nest
