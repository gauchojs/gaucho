# @gaucho/nest

Usefull module to work with mongoose and redis

## Installation

```bash
$ npm i --save @gaucho/shared \
               @gaucho/nest \
               @nestjs/mongoose \
               mongoose \
               ioredis
```

# The modules

- [RedisModule](REDIS.md)
- [RepositoryModule](REPOSITORY.md)
- [StorageModule](STORAGE.md)

# Related

- [KeycloakModule](https://www.npmjs.com/package/@gaucho/nest-keycloak)
