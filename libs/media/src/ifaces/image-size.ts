import { ImageBreakpoint } from '@gaucho/shared';

export type ImageSizes = {
  [breakpoint in ImageBreakpoint]: number;
};;
