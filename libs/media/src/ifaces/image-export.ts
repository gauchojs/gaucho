export type ImageType = 'jpeg' | 'png' | 'webp' | 'gif' | 'tiff' | 'heif' | 'raw' | 'tile';

export interface ImageExport {
  quality: number;
  type: ImageType;
}
