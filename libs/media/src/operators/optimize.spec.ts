import * as path from 'path';
import * as fs from 'fs-extra';
import * as mime from 'mime';
import { BufferedFile, ImageBreakpoint } from '@gaucho/shared';
import { optimize } from './optimize';
import { concatMap, switchMap, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';

describe('operators/optimize', () => {

  let file: BufferedFile;
  const output = path.resolve(__dirname, '../test/output');

  beforeAll(async () => {
    await fs.remove(output);
    await fs.mkdir(output);

    const source = path.resolve(__dirname, '../test/tiger.jpg');
    const buffer = await fs.readFile(source);
    const stat = await fs.stat(source);
    const ext = path.extname(source);
    // Imitate a uploaded file
    file = {
      fieldname: 'file',
      originalname: 'tiger.jpg',
      encoding: '7bit',
      mimetype: mime.getType(ext),
      size: stat.size,
      buffer,
    };

  });

  it(`should optimize a image`, async (done) => {

    const breakpoints: ImageBreakpoint[] = ['avatar', 'thumbnail', 'xs', 'sm', 'md', 'lg', 'xl'];

    await optimize(file, breakpoints).pipe(
      concatMap(item => {
        const oname = path.join(output, `${item.breakpoint}@output.jpg`);
        return from(fs.writeFile(oname, item.buffer));
      })
    ).toPromise();

    const filenames$ = from(breakpoints).pipe(
      switchMap(breakpoint => of(path.join(output, `${breakpoint}@output.jpg`))),
    );

    // exists
    await filenames$.pipe(
      switchMap(filename => from(fs.pathExists(filename))),
      tap(exists => expect(exists).toBe(true)),
    ).toPromise();

    // sizes
    await filenames$.pipe(
      switchMap(filename => from(fs.stat(filename))),
      tap(stat => expect(stat.size < file.size).toBe(true)),
    ).toPromise();

    done();

  })

})
