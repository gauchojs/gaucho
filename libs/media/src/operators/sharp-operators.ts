import { OperatorFunction, of, Observable, from } from 'rxjs';
import {
  Sharp,
  ResizeOptions,
  OutputOptions,
  JpegOptions,
  PngOptions,
  WebpOptions,
  TiffOptions,
  AvailableFormatInfo
} from 'sharp';
import { switchMap } from 'rxjs/operators';
import { BufferedFile } from '@gaucho/shared';
import sharp from 'sharp';
import { ImageType } from '../ifaces';

export function create(file: BufferedFile): Observable<Sharp> {
  return of(sharp(file.buffer));
}

export function resize(width?: number | null, height?: number | null, options?: ResizeOptions): OperatorFunction<Sharp, Sharp> {
  return input$ => input$.pipe(
    switchMap(target => of(target.resize(width, height, options))),
  );
}

export function toFormat(format: ImageType | AvailableFormatInfo, options?: OutputOptions | JpegOptions | PngOptions | WebpOptions | TiffOptions): OperatorFunction<Sharp, Sharp> {
  return input$ => input$.pipe(
    switchMap(target => of(target.toFormat(format, options))),
  );
}

export function toBuffer(options?: { resolveWithObject: false }): OperatorFunction<Sharp, Buffer> {
  return input$ => input$.pipe(
    switchMap(target => from(target.toBuffer(options)) ),
  );
}
