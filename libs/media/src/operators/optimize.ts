import { OperatorFunction, of, Observable,forkJoin, from, BehaviorSubject, ReplaySubject } from 'rxjs';
import { switchMap, concatMap, reduce, shareReplay, tap } from 'rxjs/operators';
import { Sharp } from 'sharp';
import { BufferedFile } from '@gaucho/shared';
import { ImageType, ImageSizes } from '../ifaces';
import { create, toBuffer, resize, toFormat } from './sharp-operators';
import { ImageBreakpoint } from '@gaucho/shared';

const defaultImageSizes: ImageSizes = {
  'avatar': 50,
  'thumbnail': 720,
  'xs': 480,
  'sm': 720,
  'md': 1140,
  'lg': 1920,
  'xl': 3840,
};

function process(
  breakpoint: ImageBreakpoint,
  exportType: ImageType = 'jpeg',
  quality = 70,
  sizes: ImageSizes = defaultImageSizes
): OperatorFunction<Sharp, { breakpoint: ImageBreakpoint, buffer: Buffer, exportType: ImageType }> {
  const width: number = sizes[breakpoint];
  const height: number = breakpoint === 'avatar' ? sizes[breakpoint] : null;
  return input$ => input$.pipe(
    resize(width, height),
    toFormat(exportType, { quality }),
    toBuffer(),
    switchMap(buffer => of({ breakpoint, buffer, exportType })),
  );
}

function optimizeBy(
  target: Observable<Sharp>,
  exportType: ImageType = 'jpeg',
  quality = 70,
  sizes: ImageSizes = defaultImageSizes
): OperatorFunction<ImageBreakpoint, { breakpoint: ImageBreakpoint, buffer: Buffer, exportType: ImageType }> {
  return input$ => input$.pipe(
    concatMap(breakpoint =>
      target.pipe(
        process(breakpoint, exportType, quality, sizes)
      )
    ),
  );
}

/**
 * Optimize the images by breakpoints
 *
 * @param file
 * @param breakpoints
 * @param type
 * @param sizes
 */
export function optimize(
    file: BufferedFile,
    breakpoints: ImageBreakpoint[],
    exportType: ImageType = 'jpeg',
    quality = 70,
    sizes: ImageSizes = defaultImageSizes
  ): Observable<{ breakpoint: ImageBreakpoint, buffer: Buffer, exportType: ImageType }> {
  // share the same Sharp
  const target = create(file);
  return from(breakpoints).pipe(
    optimizeBy(target, exportType, quality, sizes),
  );

}
