# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.19](https://gitlab.com/gauchojs/gaucho/compare/v0.0.18...v0.0.19) (2020-09-17)

**Note:** Version bump only for package @gaucho/media





## [0.0.18](https://gitlab.com/gauchojs/gaucho/compare/v0.0.17...v0.0.18) (2020-09-01)

**Note:** Version bump only for package @gaucho/media





## [0.0.17](https://gitlab.com/gauchojs/gaucho/compare/v0.0.16...v0.0.17) (2020-09-01)

**Note:** Version bump only for package @gaucho/media





## [0.0.16](https://gitlab.com/gauchojs/gaucho/compare/v0.0.15...v0.0.16) (2020-09-01)

**Note:** Version bump only for package @gaucho/media
