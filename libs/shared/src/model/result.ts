import { Entity } from './entity';

export type MetadataType = 'single' | 'collection';

export class BaseResult {
  links: any;
  included: Entity<any>[];
  errors: any[];

  appendInclude(...includes: Entity<any>[]) {
    if (!this.included) {
      this.included = [];
    }

    this.included.push(...includes);
  }

  appendError(...errors: any[]) {
    if (!this.errors) {
      this.errors = [];
    }

    this.errors.push(...errors);
  }

}

export class SingleMetadata {

  type: MetadataType = 'single';

  constructor(public readonly took: number) {}

}

export class CollectionMetadata {

  type: MetadataType = 'collection';
  next: number;
  prev: number;
  pages: number;

  get hasNext(): boolean {
    return this.next !== null;
  }
  get hasPrev(): boolean {
    return this.prev !== null;
  }

  constructor(
    public count?: number,
    public took?: number,
    public page?: number,
    public size?: number,
  ) {
    this.initialize();
  }

  initialize() {
    this.pages = (this.size > 0) ? (Math.ceil(this.count / this.size) || 1) : null;
    this.prev = this.page > 1 ? this.page - 1 : null;
    this.next = this.page < this.pages ? this.page + 1 : null;
  }

}

export class CollectionResult<T> extends BaseResult {

  meta: CollectionMetadata;

  constructor(
    public readonly data: Entity<T>[] = [],
    took: number,
    page?: number,
    size?: number,
    totalDocs?: number,
  ) {
    super();
    if (data) {
      this.meta = new CollectionMetadata(totalDocs, took, page, size);
    }
  }

}

export class SingleResult<T> extends BaseResult  {

  meta: SingleMetadata;

  constructor(
    public readonly data: Entity<T> = null,
    took: number,
  ) {
    super();
    this.meta = new SingleMetadata(took);
  }

}
