export interface EntityBase {
  createdAt?: Date;
  updatedAt?: Date;
}

export interface Relationship {
  type: string;
  id: string;
}

export interface Entity<T> extends EntityBase {
  type: string;
  id: string;
  attributes: Partial<T>;
  relationships?: { [attribute:string]: Relationship }
};
