
export interface Query {
  type: string;
  filter?: any,
  sort?: string;
  select?: string;
  populate?: string;
  page?: number;
  size?: number;
}

export interface SingleQuery {
  type: string;
  id: any,
  select?: string;
  populate?: string;
}

export interface IncludeQuery {
  relationship?: string;
  select?: string;
  populate?: string;
}
