export interface StorageSource {
  filename: string,
  size: number,
  breakpoint?:  string,
}

export interface StorageFile {
  type: string;
  originalName: string;
  bucketName: string;
  filename: string;
  relative: string;
  size: number;
  sources?: StorageSource[];
  temporal: boolean;
  expiration?: Date;
  // virtuals
  totalSize?: number;
  url?: string;
  sourcesUrls?: string[];
}
