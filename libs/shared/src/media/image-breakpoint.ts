export enum ImageBreakpointEnum {
  Avatar = 'avatar',
  Thumbnail = 'thumbnail',
  XS = 'xs',
  SM = 'sm',
  MD = 'md',
  LG = 'lg',
  XL = 'xl',
}

export type ImageBreakpoint = 'avatar' | 'thumbnail' | 'xs' | 'sm' | 'md' | 'lg' | 'xl';
