# @gaucho/shared

Interfaces shared between by @gaucho/nest and @gaucho/angular

## Running unit tests

Run `ng test shared` to execute the unit tests via [Jest](https://jestjs.io).
