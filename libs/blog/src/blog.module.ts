import { Module, Global } from '@nestjs/common';

import { BlogController } from './blog.controller';

@Global()
@Module({
  controllers: [BlogController],
  providers: [],
  exports: [],
})
export class BlogModule {}
