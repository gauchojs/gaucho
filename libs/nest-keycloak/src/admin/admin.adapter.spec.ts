import { KeycloakConfigAdapter } from "../adapters/config.adapter";
import { KeycloakAdminAdapter } from './admin.adapter';
import KcAdminClient from 'keycloak-admin';

describe('KeycloakAdminAdapter', () => {

  const ops = {
    serverUrl: 'http://localhost:8080/auth',
    adminPwd: 'admin'
  };

  it(`should get a usable Keycloak admin client`, async () => {

    const cadapter = new KeycloakConfigAdapter(ops);

    const config = await cadapter.resolve();

    const adapter = new KeycloakAdminAdapter(config);
    expect(adapter instanceof KeycloakAdminAdapter).toBe(true);

    const client = await adapter.client();
    expect(client instanceof KcAdminClient).toBe(true);

  })


});
