import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakAdminModule } from './keycloak-admin.module';
import KcAdminClient from 'keycloak-admin';
import { KeycloakAdminAdapter } from './admin.adapter';
import { KeycloakAdminService } from './keycloak-admin.service';

describe('KeycloakAdminModule', () => {

  let service: KeycloakAdminService;
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      imports: [
        KeycloakAdminModule.forRoot({
          serverUrl: 'http://localhost:8080/auth',
          adminPwd: 'admin'
        }),
      ],
    }).compile();

    app.useLogger(null);

    service = app.get<KeycloakAdminService>(KeycloakAdminService);
  });

  afterAll(async () => {
    await app.close();
  });

  it('should create an usable KeycloakAdminService', async () => {
    expect(typeof service).toBe('object');
    expect(service instanceof KeycloakAdminAdapter).toBe(true);

    const client = await service.client();
    expect(client instanceof KcAdminClient).toBe(true);

    const users = await client.users.find();
    expect(users instanceof Array).toBe(true);

    const rusers = await client.users.find({ realm: 'gaucho' });
    expect(rusers instanceof Array).toBe(true);

  });

})
