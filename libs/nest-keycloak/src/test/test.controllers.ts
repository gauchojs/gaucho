import { Controller, Get } from '@nestjs/common';
import { Authenticated, User } from '../decorators';

@Controller('private')
export class PrivateController {

  @Get()
  @Authenticated()
  get() {
    return { data: 'OK' };
  }

  @Get('user')
  @Authenticated()
  getUser(@User() user) {
    return { username: user['username'] };
  }

  @Get('role')
  @Authenticated('test-guest')
  oneRole() {
    return { data: 'OK' };
  }

  @Get('roles')
  @Authenticated('test-admin', 'test-superadmin')
  multiRole() {
    return { data: 'OK' };
  }

  @Get('admin')
  @Authenticated('test-admin')
  admin() {
    return { data: 'OK' };
  }

}

@Controller('public')
export class PublicController {

  @Get()
  get() {
    return { data: 'OK' };
  }

  @Get('user')
  getUser(@User() user) {
    return { username: user['username'] };
  }

}

@Controller('admin')
@Authenticated('test-admin')
export class AdminController {

  @Get()
  getUser(@User() user) {
    return { username: user['username'] };
  }

  @Get('override')
  @Authenticated('test-guest')
  override() {
    return { data: 'OK' };
  }

}
