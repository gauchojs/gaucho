import KcAdminClient from 'keycloak-admin';

export class TestRoleFactory {

  id: string;

  constructor(
    public roleName: string,
    public realm: string,
    public prefix: string
  ) {
    this.roleName = `${prefix}-${roleName}`;
  }

  async fetch(client: KcAdminClient): Promise<boolean> {
    const krol = await client.roles.findOneByName({ name: this.roleName, realm: this.realm });
    if (krol) {
      this.id = krol.id;
      return true;
    }
    return false;
  }

  async create(client: KcAdminClient): Promise<void> {
    const exists = await this.fetch(client);
    if (exists) {
      return;
    }
    await client.roles.create({ name: this.roleName, realm: this.realm });
    // fecth again to get the id
    await this.fetch(client);
  }

  async delete(client: KcAdminClient): Promise<void> {
    const exists = await this.fetch(client);
    if (!exists) {
      return;
    }
    await client.roles.delByName({ name: this.roleName, realm: this.realm });
  }

  map(): { id: string, name: string } {
    return { id: this.id, name: this.roleName };
  }

}
