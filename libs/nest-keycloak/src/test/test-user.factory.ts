import KcAdminClient from 'keycloak-admin';
import { TestRoleFactory } from './test-role.factory';

export class TestUserFactory {

  id: string;

  accessToken: string;

  constructor(
    public username: string,
    public realm: string,
    public prefix: string,
    public roles: TestRoleFactory[] = [],
  ) {
    this.username = `${prefix}-${username}`;
  }

  async fetch(client: KcAdminClient): Promise<boolean> {
    const kusers = await client.users.find({ username: this.username, realm: this.realm });
    if (kusers.length === 0) {
      return false;
    }

    const kuser = kusers[0];
    this.id = kuser.id;
    return true;
  }

  async create(client: KcAdminClient): Promise<void> {
    const exists = await this.fetch(client);
    if (exists) {
      return;
    }
    const {id} = await client.users.create({
      realm: this.realm,
      username: this.username,
      email: `${this.username}@${this.realm}`,
      emailVerified: true,
      enabled: true
    });
    this.id = id;
    await client.users.resetPassword({
      realm: this.realm,
      id: id,
      credential: {
        temporary: false,
        type: 'password',
        value: this.username,
      },
    });
    await this.asignRoles(client);
  }

  async delete(client: KcAdminClient): Promise<void> {
    const exists = await this.fetch(client);
    if (!exists) {
      return;
    }
    await client.users.del({ id: this.id, realm: this.realm });
  }

  private async asignRoles(client: KcAdminClient): Promise<void> {
    if (!this.roles || this.roles.length === 0) {
      return;
    }
    await client.users.addRealmRoleMappings({
      id: this.id,
      roles: this.roles.map(role => role.map()),
      realm: this.realm
    });
  }

  setToken(token: string) {
    this.accessToken = token;
  }

}
