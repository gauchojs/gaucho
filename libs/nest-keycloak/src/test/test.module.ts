import { Global, Module } from "@nestjs/common";
import { PrivateController, PublicController, AdminController } from './test.controllers';

@Global()
@Module({
  controllers: [
    PrivateController,
    PublicController,
    AdminController,
  ]
})
export class TestKeyclaokModule { }
