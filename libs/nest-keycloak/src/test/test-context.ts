import { KeycloakAdminOptions } from '../ifaces';
import { TestRoleFactory } from './test-role.factory';
import { TestUserFactory } from './test-user.factory';
import { from } from 'rxjs';
import { tap, concatMap } from 'rxjs/operators';
import { KeycloakConfigAdapter } from '../adapters';
import KcAdminClient from 'keycloak-admin';
import { KeycloakAdminAdapter } from '../admin';

export class KeycloakTestContext {

  private ops: KeycloakAdminOptions;

  private roles: TestRoleFactory[];

  private users: TestUserFactory[];

  private adapter: KeycloakAdminAdapter;

  constructor(
    options: KeycloakAdminOptions,
    private realm: string,
    private prefix: string = 'test',
  ) {
    this.ops = new KeycloakAdminOptions(options);
    this.createFactories();
  }

  private createFactories() {
    this.roles = ['guest', 'admin', 'superadmin'].map(
      roleName => new TestRoleFactory(roleName, this.realm, this.prefix)
    );

    this.users = [
      new TestUserFactory(
        'demo',
        this.realm,
        this.prefix
      ),
      new TestUserFactory(
        'guest',
        this.realm,
        this.prefix,
        [
          this.getRole('guest')
        ]
      ),
      new TestUserFactory(
        'admin',
        this.realm,
        this.prefix,
        [
          this.getRole('admin'),
          this.getRole('superadmin'),
        ]
      ),
    ];
  }

  getRole(roleName: string): TestRoleFactory {
    return this.roles.find(role => role.roleName === `${this.prefix}-${roleName}`);
  }

  getUser(username: string): TestUserFactory {
    return this.users.find(user => user.username === `${this.prefix}-${username}`);
  }

  async prepare(): Promise<void> {
    const adapter = await this.getAdapter();
    const client = await adapter.client();

    await from(this.roles).pipe(
      concatMap(async role => await role.create(client))
    ).toPromise();

    await from(this.users).pipe(
      concatMap(async user => await user.create(client))
    ).toPromise();

    await from(this.users).pipe(
      concatMap(async user => {
        const cli = new KcAdminClient({
          baseUrl: this.ops.serverUrl,
          realmName: this.realm
        });
        await cli.auth({
          username: user.username,
          password: user.username,
          grantType: 'password',
          clientId: 'admin-cli',
        });
        user.setToken(cli.accessToken);
      })
    ).toPromise();

  }

  async clean(): Promise<void> {
    const adapter = await this.getAdapter();
    const client = await adapter.client();

    await from(this.users).pipe(
      tap(async user => await user.delete(client))
    ).toPromise();

    await from(this.roles).pipe(
      tap(async role => await role.delete(client))
    ).toPromise();

  }

  private async getAdapter(): Promise<KeycloakAdminAdapter> {
    try {
      if (!this.adapter) {
        const cadapter = new KeycloakConfigAdapter(this.ops);
        const config = await cadapter.resolve();
        this.adapter = new KeycloakAdminAdapter(config);
      }
      return this.adapter;
    } catch (error) {
      console.log(error)
      throw error;
    }
  }

}
