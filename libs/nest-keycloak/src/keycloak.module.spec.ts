import request from 'supertest';
import { Test } from '@nestjs/testing';
import { KeycloakModule } from './keycloak.module';
import { INestApplication } from '@nestjs/common';
import { KeycloakTestContext, TestKeyclaokModule } from './test';

describe('KeycloakModule', () => {

  const keycloakUrl = 'http://localhost:8080/auth';

  let app: INestApplication;
  const context = new KeycloakTestContext({
    serverUrl: keycloakUrl,
    adminPwd: 'admin'
  }, 'gaucho');

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        KeycloakModule.forRoot({
          serverUrl: keycloakUrl,
          realm: 'gaucho',
          clientId: 'admin-cli',
          session: null, // wrong
        }),
        TestKeyclaokModule
      ]
    }).compile();

    app = moduleRef.createNestApplication();

    app.useLogger(null);
    await app.init();

    await context.prepare();
  });

  describe('Decorators', () => {

    it(`KeycloakModule should not interfier withour @Authorized()`, () => {
      return request(app.getHttpServer())
        .get('/public')
        .expect(200)
        .expect({
          data: 'OK',
        });
    })

    it(`@Authorized() should unauthorized without token`, () => {
      return request(app.getHttpServer())
        .get('/private')
        .expect(401);
    })

    it(`@Authorized() should authorized with a token`, () => {
      const user = context.getUser('demo');
      return request(app.getHttpServer())
        .get('/private')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200)
        .expect({
          data: 'OK',
        });
    })

    it(`@Authorized('guest') should authorized a 'guest' role`, () => {
      const user = context.getUser('guest');
      return request(app.getHttpServer())
        .get('/private/role')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200)
        .expect({
          data: 'OK',
        });
    })

    it(`@Authorized('admin', 'superadmin') should authorized the roles 'admin' or 'superadmin'`, () => {
      const user = context.getUser('admin');
      return request(app.getHttpServer())
        .get('/private/roles')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200)
        .expect({
          data: 'OK',
        });
    })

    it(`@Authorized('admin') should unauthorized 'guest' role`, () => {
      const user = context.getUser('guest');
      return request(app.getHttpServer())
        .get('/private/admin')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(401);
    })

    it(`@User() should retrive a user 'demo'`, () => {
      const user = context.getUser('demo');
      return request(app.getHttpServer())
        .get('/private/user')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200)
        .expect({
          username: user.username
        });
    })

    it(`@User() should retrive a anonymous without a token`, () => {
      return request(app.getHttpServer())
        .get('/public/user')
        .expect(200)
        .expect({
          username: 'anonymous',
        });
    })

    it(`@User() should retrive a user with a token`, () => {
      const user = context.getUser('demo');
      return request(app.getHttpServer())
        .get('/public/user')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200)
        .expect({
          username: user.username
        });
    })

    it(`@Authorized('admin') on a class should unauthorized 'guest' role`, () => {
      const user = context.getUser('guest');
      return request(app.getHttpServer())
        .get('/admin')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(401);
    })

    it(`@Authorized('admin') on a class should authorized 'admin' role`, () => {
      const user = context.getUser('admin');
      return request(app.getHttpServer())
        .get('/admin')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200)
        .expect({
          username: user.username
        });
    })

    it(`@Authorized('admin')class and @Authorized('guest')method should override 'admin' role`, () => {
      const user = context.getUser('guest');
      return request(app.getHttpServer())
        .get('/admin/override')
        .set('authorization', `bearer ${user.accessToken}`)
        .expect(200);
    })

  })

  afterAll(async () => {
    await app.close();
    await context.clean();
  });

})
