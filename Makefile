VERSION=$(shell echo `node -pe 'require("./package.json").version'`)
RUNNER=$(shell whoami)

N=[0m
R=[00;31m
G=[01;32m
Y=[01;33m
B=[01;34m
L=[01;30m

commands: welcome
	@echo "${B}Available commands"
	@echo ""
	@echo "    ${G}docker_up${N}         Init docker compose."
	@echo "    ${G}docker_down${N}       Down docker compose."
	@echo "    ${G}test${N}              Test the project."
	@echo "    ${G}build${N}             Build the project."
	@echo "    ${G}publish${N}           Publish the project."
	@echo ""

welcome:
	@echo ":: ${Y}gaucho@${VERSION}${N} as ${RUNNER}"
	@echo ""

init: welcome

docker_up:
	# sudo sysctl -w vm.max_map_count=262144
	@docker-compose  \
    -f docker/mongo.yaml \
    -f docker/redis.yaml \
    -f docker/keycloak.yaml \
    -f docker/storage.yaml \
    -p gaucho up -d

docker_down:
	@docker-compose  \
    -f docker/mongo.yaml \
    -f docker/redis.yaml \
    -f docker/keycloak.yaml \
    -f docker/storage.yaml \
    -p gaucho down

test:
	@nx test

build:
	lerna version --conventional-commits --yes
	@nx build shared
	@nx build media
	@nx build nest
	@nx build nest-keycloak
	@nx build angular --prod && yarn bundle-styles

publish: test build
	@npm publish dist/libs/shared --access public
	@npm publish dist/libs/media --access public
	@npm publish dist/libs/nest --access public
	@npm publish dist/libs/nest-keycloak --access public
	@npm publish dist/libs/angular --access public


.PHONY: commands
