# Run Docker

Up with docker-compose

```
docker-compose  \
  -f docker/mongo.yaml \
  -f docker/redis.yaml \
  -f docker/keycloak.yaml \
  -f docker/storage.yaml \
  -p gaucho up -d
```

Down with docker-compose

```
docker-compose  \
  -f docker/mongo.yaml \
  -f docker/redis.yaml \
  -f docker/keycloak.yaml \
  -f docker/storage.yaml \
  -p gaucho down
```
