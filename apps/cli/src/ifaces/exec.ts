export interface ExecResult {
  code: number;
  stdout?: string[],
  stderr?: string[],
}
