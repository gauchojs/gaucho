export enum BackupType {
  Mysql = 'mysql',
  Postgresql = 'postgresql',
  MongoDB = 'mongodb',
}
