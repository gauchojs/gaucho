import * as child_process from 'child_process';
import { ExecResult } from '../ifaces';
const { spawn } = child_process;

export class ChildExecutor {

  cwd = process.cwd();

  constructor(basedir?) {
    if (basedir) {
      this.cwd = basedir;
    }
  }

  async exec(command: string, args?: string[]): Promise<ExecResult> {
    return new Promise<ExecResult>((res, rej) => {
      const cmd = spawn(command, args);
      const stdout: string[] = [];
      const stderr: string[] = [];

      cmd.stdout.on('data', (data) => {
        stdout.push(data.toString());
      });

      cmd.stderr.on('data', (data) => {
        stderr.push(data.toString());
      });

      cmd.on('close', (code) => {
        res({ code, stdout, stderr });
      });

    });
  }

}
