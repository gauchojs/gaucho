import * as yargs from 'yargs';

// imports cmds
import { commands } from './command';

export default async function run() {
  try {
    const cli = yargs.usage('gaucho <command> [flags]');

    commands.forEach((cmd : any) => {
      cli.command(cmd);
    });

    return cli.demandCommand().help().argv;
  } catch (error) {
    console.log(`[ERROR] ${error.message}`);
    process.exit(1);
  }
}

run();
