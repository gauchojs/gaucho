import { GauchoModule } from '@gaucho/nest';
import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    GauchoModule.forRoot({
      db: {
        uri: 'mongodb://localhost:27017/blog-dev'
      },
      auth: {
        serverUrl: 'http://localhost:8080/auth',
        realm: 'gaucho',
        clientId: 'admin-cli',
      },
      // storage?: StorageModuleOptions,
      session: {
        secret: 'someUngly',
        redisUrl: 'redis://localhost:6379',
        keyPrefix: 'session',
        name: 'demo'
      }
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
