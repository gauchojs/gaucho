import { Logger } from '@nestjs/common';
import { AppModule } from './app/app.module';

import { GauchoAppOptions, GauchoFactory } from '@gaucho/nest';

async function bootstrap() {

  const appOtions: GauchoAppOptions = {
    apiPrefix: 'v1',
    session: {
      secret: 'someUngly',
      redisUrl: 'redis://localhost:6379',
      keyPrefix: 'session',
      name: 'demo'
    },
    env: 'development',
  };


  const port = process.env.PORT || 8000;
  const app = await GauchoFactory.create(AppModule, appOtions);

  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + appOtions.apiPrefix);
  });
}

bootstrap();
