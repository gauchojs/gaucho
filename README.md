# Gaucho (Devel stage)

Core libraries to make [Nest](https://nestjs.com/) + [Angular](https://angular.io/) applications

## Environment approach

![](environment-approach.png)

# Nest modules

- [KeycloakModule](./libs/nest-keycloak/README.md)
- [RepositoryModule](./libs/nest/REPOSITORY.md)
- [RedisModule](./libs/nest/REDIS.md)
- [StorageModule](./libs/nest/STORAGE.md)
- [MediaModule](./libs/media/README.md)

# Angular modules

- [NggModule](./libs/angular/README.md)

# Developers

To run this project locally see:
- [docker guide](./docker/README.md)

Or just run:
```
make docker_up
```

To down the docker containers:
```
make docker_down
```

# Test

To test the project

```
make docker_up
make test
```

## References

- This project is managed using [Nx](https://nx.dev)
- Backend by [NestJS](https://nestjs.com/)
- Authentication & Authorization by [keycloak](https://www.keycloak.org/)
- Database by [MongoDB](https://www.mongodb.com/)
- Indexation by [Elasticsearch](https://www.elastic.co/es/what-is/elasticsearch)
- Cache/Session data by [Redis](https://redis.io/)
- Storage by [Minio](https://min.io/)
- Frontend libraries by [Angular](https://angular.io/)
 
## License

Gaucho is [MIT licensed](LICENSE).
